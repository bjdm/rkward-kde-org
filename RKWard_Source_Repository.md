---
layout: page
---

# RKWard development source repository / development snapshots

Instructions to track the most recent development version in our source
repository (git). This is of interest both to people wanting to be on
the bleeding edge of RKWard development, and to people wanting to
contribute patches.

## Alternatives: Daily builds

Sometimes all you want is to test a relatively recent development
snapshot, easily.

  - For Ubuntu users, a variety of PPAs is available at [https://launchpad.net/~rkward-devel](https://launchpad.net/~rkward-devel), including the following daily build archives:
      - [KF5 version of RKWard](https://launchpad.net/~rkward-devel/+archive/ubuntu/rkward-kf5-daily)
      - [KF5 version of RKWard compiled for CRAN R](https://launchpad.net/~rkward-devel/+archive/ubuntu/rkward-kf5-daily-r40)
      - [KF5 version of RKWard compiled for CRAN R and KF5 backports](https://launchpad.net/~rkward-devel/+archive/ubuntu/rkward-kf5-backports-daily-r40)
  - For Windows users, daily builds can be found at <https://binary-factory.kde.org/view/Windows%2064-bit/job/RKWard_Nightly_win64/>
  - Daily builds for MacOS are at: <https://binary-factory.kde.org/view/MacOS/job/RKWard_Nightly_macos/>

## Pre-requisites

In addition to the requirements listed on the
[Building_RKWard_From_Source](Building_RKWard_From_Source.html)
page, you will need a git client.

## Checking out the current Git sources

For anoynmous (read-only) access, use

`$ git clone https://invent.kde.org/education/rkward.git rkward_devel`

This will download the development sources to a directory called
**rkward_devel**.

## Compiling

Proceed with compilation / installation as usual (see [Building RKWard
From Source](Building_RKWard_From_Source.html)). Make sure the
path provided to cmake is correct (for example, depending on where you
create the build directory, **'../rkward**' may be appropriate instead
of **'..**').

## Staying up to date

To update your working copy to the most recent changes, go to that
directory, and run

`$ git pull --rebase`

After this, generally only the

`$ make`

and

`$ sudo make install`

steps are needed (less, if you use some advanced tricks).

## Producing patches

[Submitting patches](Patches.html)

## Source browsing

You can also browse the Git repository online:
<https://invent.kde.org/education/rkward>

