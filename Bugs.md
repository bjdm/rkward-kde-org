---
layout: page
---

# Reporting bugs or wishes

Thank you for taking the time to report things that are broken,
missing, or should be improved, in RKWard. Before you do, **a few basic
guides to make sure we can handle your input, efficiently**. We'll make it
short:

## Browsing and commenting on existing bugs and wishes

Especially after new releases of RKWard and/or R we occasionally get
reports of similar problems repeatedly. Please take a minute to check, whether the issue you are about to report
has already been reported (and probably fixed, too). You can browse existing [open bugs](https://bugs.kde.org/buglist.cgi?bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&product=rkward&query_format=advanced)
and
[wishes](https://bugs.kde.org/buglist.cgi?bug_severity=wishlist&bug_severity=task&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=ASSIGNED&bug_status=REOPENED&product=rkward&query_format=advanced)
in the KDE bugtracking system.

## Reporting bugs

Please make sure you provide us with as much helpful information as you can. Even seemingly "obvious" issues will sometimes depend on very specific circumstances that we need to understand to be able to address the problem at all.
We often have to spend a considerable amount of time asking for basic information, and so you can save us (and yourself) a lot of work by providing the most important information upfront.

  - **Whenever possible, use the template provided by `Help-\>Report Bug`
    from within RKWard**. This will automatically include a lot of
    useful information, that is often key to analysing issues.
  - If this is not possible, for some reason (e.g. RKWard is not
    starting at all), make sure to include information on the following
    items:
    - the **version number of RKWard**
    - the **version number of R**
    - the **version number of KDE frameworks (libraries)**
    - the **operating system**
    - the **method of installation** used (for KDE, R and RKWard; e.g.
    binary bundle, distributor's packages or compiled from source)
    - Much of the above is also provided by running `rk.sessionInfo()` in the R Console window.
  - Make sure to answer the following questions **very diligently:**
    - **what did you do** to run into the problem? (if it's running some R
    code, please consider including example code to reproduce it)
    - how **can the problem be reproduced**? Please try to be as specific as possible. Sometimes a bug will depend on a minor detail of your workflow, so we need to be able follow your *exact* steps.

**Submit this info as a [new bug
report](https://bugs.kde.org/enter_bug.cgi?alias=&assigned_to=&attach_text=&blocked=&bug_file_loc=http%3A%2F%2F&bug_severity=normal&comment=---%20Please%20fill%20in%20---%0D%0A---%20IMPORTANT%20NOTE%3A%20If%20possible%20at%20all%2C%20please%20use%20the%20%22Report%20bug%22%20feature%20from%20the%20%22Help%22%20menu%20inside%20RKWard.%20Otherwise%2C%20please%20make%20sure%20to%20read%20http%3A%2F%2Frkward.kde.org%2Fbugs%20for%20guidelines%20on%20what%20information%20to%20include%20in%20your%20report.%20---&contenttypeentry=&contenttypemethod=autodetect&contenttypeselection=text%2Fplain&data=&dependson=&description=&flag_type-20=X&flag_type-21=X&flag_type-23=X&flag_type-24=X&flag_type-4=X&form_name=enter_bug&keywords=&maketemplate=Remember%20values%20as%20bookmarkable%20template&op_sys=Linux&product=rkward&rep_platform=Other&requestee_type-20=&requestee_type-21=&requestee_type-4=&short_desc=&version=unspecified)**
in the KDE Bugtracking System. Note that you will have to [create an
account](https://bugs.kde.org/createaccount.cgi), if you don't have one,
yet.

Only if this is not possible for some reason, you can also send your
report to our [mailing list](Contact.html).

## Reporting wishes

  - Please report one wish at a time. Split several issues into several
    tickets. This makes it much easier to manage.
  - Try to make a specific suggestion, not just "Feature XY should be
    improved". If you cannot quite put your finger on what should be
    done, do let us know, too, but write to our [mailing
    list](Contact.html), instead of opening a ticket. This is
    better suited for discussions.
  - While in many cases this information may not be strictly necessary,
    it is never wrong, and often helpful, to include the output of
    `rk.sessionInfo()` in your request.

**Subject to the above, submit this info as a [new wishlist
item](https://bugs.kde.org/enter_bug.cgi?alias=&assigned_to=&attach_text=&blocked=&bug_file_loc=http%3A%2F%2F&bug_severity=wishlist&comment=---%20Please%20fill%20in%20---%0D%0A---%20IMPORTANT%20NOTE%3A%20Please%20make%20sure%20to%20read%20http%3A%2F%2Frkward.kde.org%2Fbugs%20for%20guidelines%20on%20what%20information%20to%20include%20in%20your%20report.%20---&contenttypeentry=&contenttypemethod=autodetect&contenttypeselection=text%2Fplain&data=&dependson=&description=&flag_type-20=X&flag_type-21=X&flag_type-23=X&flag_type-24=X&flag_type-4=X&form_name=enter_bug&keywords=&maketemplate=Remember%20values%20as%20bookmarkable%20template&op_sys=Linux&product=rkward&rep_platform=Other&requestee_type-20=&requestee_type-21=&requestee_type-4=&short_desc=&version=unspecified)**
in the KDE Bugtracking system. Note that you will have to [create an
account](https://bugs.kde.org/createaccount.cgi), if you don't have one,
yet.

Only if this is not possible for some reason, you can also send your
report to our [mailing list](Contact.html).
