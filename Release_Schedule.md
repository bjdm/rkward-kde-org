---
layout: page
---

# Release Schedule

<!-- **No release is currently scheduled.** This page shows the schedule for RKWard 0.7.3, which has been released on April 21, 2022. -->
This page shows the schedule for RKWard 0.7.4, which is planned to be released on May 30, 2022.

## Note

All dates on this page are subject to change with or without prior
notice. We put up this schedule to coordinate release efforts, but not
to enslave ourselves to a plan.

## RKWard 0.7.4

### Feature Plan

[Feature Plan](Feature_Plan.html)

### Development snapshots

Several options exist for following the [development version of RKWard](RKWard_Source_Repository.html).

### Tentative timeline

#### Preview release(s)

No dedicated point preview releases have been created. Automated preview builds are made available on a rolling basis.

  - Call for testing:
      - To be done
<!--       - <https://mail.kde.org/pipermail/rkward-devel/2022-April/005682.html>. -->
<!--  - [Source tar.gz](https://files.kde.org/rkward/testing/rkward-0.6.9z+0.7.0+pre1.tar.gz)
    (see [Building RKWard From Source](Building_RKWard_From_Source.html) for instructions). -->
  - Links to binary development snapshot packages (not all may be available, immediately):
      - [Ubuntu with stock version of R](https://launchpad.net/~rkward-devel/+archive/ubuntu/rkward-kf5-daily)
        or [Ubuntu with CRAN version of R](https://launchpad.net/~rkward-devel/+archive/ubuntu/rkward-kf5-daily-r40)
      - [Windows binary installer](https://binary-factory.kde.org/view/Windows%2064-bit/job/RKWard_Nightly_win64/)
        (see also [RKWard on Windows](RKWard_on_Windows.html)).
      - [MacOSX binary installation bundle](https://binary-factory.kde.org/job/RKWard_Nightly_macos/)
        (see also [RKWard on Mac](RKWard_on_Mac.html) for further information - you need to install >= R 4.2.x, separately).

#### Testing phase

There is no formal end to the testing phase, but please try to provide all substantial testing feedback as soon as possible, and before May 23, 2022.

#### Deadline for translations

The deadline for translations ends on May 23, 2022. Translations updated after this date may or may not make it into the release. See <https://l10n.kde.org> for information on how to get involved with translations. Contact us on the mailing list, if you need assistance.

#### Release candidate / packaging

Subject to the outcome of the testing phase, a 0.7.4-rc1-release will be prepared on or around May 24, 2022, and will appear in <https://files.kde.org/rkward/testing/for_packaging/>. Packagers are encouraged to start producing packages based on this release (with details to be announced on the mailing list).

The intent is to release the final version with no change with respect to the release candidate (except for the version number). Only packaging issues or grave bugs will prompt another rc-release. Minor bugs reported after the rc1-release will have to wait for the next release cycle.

**Packagers**: If you intend to publish packages before the official release, please make sure that the version number contains some indication that this is not the "official" version, yet (e.g. a "-rc1" suffix). Otherwise, if we do need to make a last-minute change to fix an important problem, this may result in different versions floating with the same version number, and serious confusion.

#### Official release

The targeted official release date for RKWard 0.7.4 is May 30, 2022.
