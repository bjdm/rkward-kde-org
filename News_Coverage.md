---
layout: page
---

# News articles about RKWard

On this page we keep a list of published news articles. Many of those
are relatively small, and basically announce the release of a new
version. The more important ones are linked in bold.

Many articles were written by people associated with the project. If you
would like to collaboratively work on articles on RKWard, you can use
the [Discussion section](Talk:News_Coverage.html) of this page.

## Status of this page: **Out of date / Incomplete**

Just to point out that the information below does **not** represent a
complete or up-to-date list of publications about RKWard. Please help us
by adding articles that you come across.

## 2006

### German

  - \[0.4.0\] <http://www.pro-linux.de/news/2006/10373.html>
  - \[0.3.6\]
    **<http://www.pro-linux.de/berichte/appfokus/rkward/rkward.html>**

### French

  - \[0.3.5\] <http://linuxfr.org/2006/04/16/20685.html>

### 404

  - \[13/02/2006\]
    <http://www.genoscope.cns.fr/agc/website/article.php3?id_article=390>
    (in French but compare 3 softwares : BioWeka, G-language and RkWard)

## 2005

### French

  - \[0.3.3\] <http://linuxfr.org/~peco/19652.html>
  - \[0.3.2\] <http://linuxfr.org/~peco/17980.html>

## 2004

### French

  - \[0.3\] <http://linuxfr.org/2004/11/12/17662.html>

### 404

  - \[0.3\] <http://www.bashprofile.net/article.php3?id_article=324>

## Scientific articles

RKWard is mentioned in at least the following articles, which have
appeared in peer-reviewed scientific journals:

  - <http://pubs.acs.org/doi/abs/10.1021/ac103277s>

## Software listings / Wikipedia

### Main Listings

  - <http://www.kde-apps.org/content/show.php?content=14880>
  - <http://freshmeat.net/projects/rkward/>
  - <http://linux.softpedia.com/get/Science-and-Engineering/Electronic-Design-Automation-EDA-/RKward-2378.shtml>

### Wikipedia

  - FR : <http://fr.wikipedia.org/wiki/RKWard>
  - EN : <http://en.wikipedia.org/wiki/RKWard>
