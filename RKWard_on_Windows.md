---
layout: page
---

# Download and install RKWard on Windows

## Lastest official release

* RKWard 0.7.4 <https://download.kde.org/stable/rkward/0.7.4/rkward-0.7.4-R-4.2.0-KF5-5.94.0.exe> (includes R 4.2.0, and version 5.94.0 of the required KF5 frameworks)

## Installing the official release

Installation of RKWard on windows should be straight-forward: Simply download and run the appropriate installer from one of the links above.

Please note that **the installer may appear to "hang" around 70% completion**. This is just a problem with the progress bar not updating. Please be patient.

## Installing development snapshots

Nightly development snapshots are available from
<https://binary-factory.kde.org/job/RKWard_Nightly_win64/> . Note that
these are built, automatically, and provided without any human testing.
Back up your data, before using these.

## Older releases that may still be of interest:

Should you wish to stick with R 4.1.x for now, you will either have to install from source (see below), or use RKWard 0.7.3:
* <https://download.kde.org/stable/rkward/0.7.3/win32/rkward-0.7.3-R-4.1.2-KF5-5.93.0.exe> (includes R 4.1.2, and version 5.93.0 of the required KF5 frameworks)

## Updating R inside the RKWard installation

Since RKWard 0.7.1, updating R is often possible without any updates to RKWard, especially, for patch level releases of R (e.g. 4.1.**3** over 4.1.**2**).

The recommended way to update R is to

1. Install R as usual, in its default installation location.
2. Open the file `[Your\RKWard\Installation\]\KDE\bin\rkward.ini`, and change
   the path for "R_executable" to either "auto" or the full path to R.exe.
3. In case of any problems revert the changes to rkward.ini.

Alternatively, you can install your new version of R to `[Your\RKWard\Installation\]KDE\libs\R`. The potential advantage is that you will not have to reinstall your R packages after updating R (although it is still recommendable to do so).

## Compiling RKWard from source on Windows

1.  Set up "craft" as detailed, here:
    <https://community.kde.org/Guidelines_and_HOWTOs/Build_from_source/Windows>
    .
2.  run

`craft -i --target=master rkward`

(--target=master is optional, and means to build the development version). This should be all you need to fetch rkward and all dependencies
(including R). In some cases (when - some - cached packages are not available) this may take several hours to complete.

3. (optionally): create an installer using:

`craft --package rkward`

To compile for a particular version of R, use (e.g.):

```
craft --target=4.1.2 r-base
craft -i rkward
```

#### Notes

  - At times, compilation using craft will fail. If the build fails on
    the R or RKWard packages themselves, let us know on the [mailing
    list](Contact.html). If the build fails at an earlier stage
    (qt / kdelibs / kdebase), you may want to ask for help on
    the KDE windows mailing list:<kde-windows@kde.org>.
  - To customize your installation of R or RKWard, edit / refer to the
    following python files
      - *CRAFT\\ROOT*\\etc\\blueprints\\locations\\craft-blueprints-kde\\extragear\\rkward\\rkward\\rkward.py
      - *CRAFT\\ROOT*\\etc\\blueprints\\locations\\craft-blueprints-kde\\binary\\_win\\r-base\\r-base.py
