---
layout: page
title: Feature Plan
---

# Feature plan

## Purpose of this page

The purpose of this page is to gather information on features that

  - are *currently* under active development, or
  - which are under active discussion, or
  - are likely to get implemented, soon.

Ideally, this page will serve to keep track of the development status,
to coordinate development efforts, and to provide a basis for discussion
and testing of new features.

However, this page is **not**:

  - a full list of current changes. Esp. trivial changes and most
    bugfixes are not listed, here. See the [Changelog in
    SVN](http://rkward.svn.sourceforge.net/viewvc/rkward/trunk/rkward/ChangeLog?view=markup).
  - a list of existing features in RKWard (but perhaps texts on this
    page can be re-used for such a list, once it exists).
  - a list of everything that is planned / desired. Although some items
    on this page are long-term tasks, the main idea of this page is to
    serve as a place to document short-term developments.

## Old feature Plans

[Feature Plan Archive](Feature_Plan_Archive.html)

## Current Development

These features are likely to become part of the next release.



## Planned Features

These features are likely to be targeted, soon, but not necessarily for
the next release.


### Standalone editor for vector and matrix data

**Description**: Standalone editor for vector and matrix data.

**Points for discussion / issues**:

**Development status**: <span style="color:red">Not started</span>.

**Testing status**: N/A

[Category:Developer
Information](Category:Developer_Information.html)
