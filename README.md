# Rkward website

## Build instruction

```
gem install bundler jekyll
bundle install --path vendor/bundle
```

## Run development

```
bundle exec jekyll serve
```

## Run production

```
bundle exec jekyll build
```

The configuration are located in `_config.yml`.
