---
layout: page
---

# Plugins under planning

This page lists planned plugins. Before starting a new plugin, please check this
page. If no entry exists for your planned plugin, yet, create a new one
(under section "Claimed". If it already exists, move it from "Unclaimed"
to "Claimed" and add your name/nick. If you have an idea for a plugin,
but don't plan on implementing it anytime soon, place under "Unclaimed".
Add some short info, if you feel it is not self-explanatory. Add
sub-categories as needed.

Once you place a plugin in SVN / submit a patch, please move it from
"Claimed" to
[Development_Status_of_Plugins](Development_Status_of_Plugins.html).

Where applicable, please add a reference to the relevant R function, as
this will probably make searching the page a lot easier.

# Unclaimed plugins (ideas)

## Import

  - Import SAS (read.ssd, read.xport {foreign})

## Export

  - Export SPSS / SAS / Stata (write.foreign {foreign})
  - Export ODF (OpenDocumentFortmat) (odfWeave{odfWeave})

# Claimed plugins

## X11

  - Add abline under X11 \> Edit
      - Claimed by: pk

## Plots

## Distributions

  - CLT plugins
      - Claimed by: pk
      - Are there more to come?

## Analysis

  - oneway.test {stats}
      - Claimed by: open

<!-- end list -->

  - prop.trend.test {stats}
      - Claimed by: open

<!-- end list -->

  - prop.test {stats}
      - Claimed by: open

<!-- end list -->

  - pairwise.prop.test {stats}
      - Claimed by: open

<!-- end list -->

  - pairwise.t.test {stats}
      - Claimed by: open

<!-- end list -->

  - friedman.test {stats}
      - Claimed by: open

<!-- end list -->

  - mantelhaen.test {stats}
      - Claimed by: open

<!-- end list -->

  - mauchly.test {stats}
      - Claimed by: open

<!-- end list -->

  - chisq.test {stats}
      - Claimed by: open

<!-- end list -->

  - mcnemar.test {stats}
      - Claimed by: open

<!-- end list -->

  - fisher.test {stats}
      - Claimed by: open

<!-- end list -->

  - anova.glm {stats}
      - Claimed by: open

<!-- end list -->

  - binom.test {stats}
      - Claimed by: open

<!-- end list -->

  - kruskal.test {stats}
      - Claimed by: open

[Category:Developer
Information](Category:Developer_Information.html)
