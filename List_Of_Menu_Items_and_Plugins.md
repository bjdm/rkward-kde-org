---
layout: page
---

# List of menu items and plugins

## **WARNING**: This page is incomplete and outdated!

The purpose of this page is to provide a rough overview of RKWard's
features, as well as a quick reference for the menu items and their use.
For this purpose we will not make a distinction between built-in
features and standard [plugins](Plugins.html). There is an extra
page where [third party plugins](Third_Party_Plugins.html) are
documented (and linked to).

This page contains only some very short descriptions. For items in need
of more detailed information, please create a new page.

## List / Documentation of Menu Items

### File

#### Save / Load

##### Save

\- Save as R object

This plug-in allow you to save a R project. Note : you can save an R
project by exiting RKWard, a window appears to ask you if you want
saving the R space project, click on yes and choose the name (do not
forgot the R extension).

\- Save package

\- Save variables

Save only one variable in a file.

##### Load

\- Load a R object

This plug-in allow you to open a R project.

\- Load data

This plug-in allow you to import CSV data from a databank or a data from
an office suite.

\- Load source file

#### Save as table

Allow export data as table (csv) to import them in office suite as csv
file. You can choose data, columns, rows, characters for decimal values,
to separate value and a lot of others options.

### Edit

### Workspace

### Output

### Run

### Analysis

#### Basic statistics

#### Correlation

##### Correlation matrix

Calculate a correlation matrix between two or more variables.

##### Correlation matrix plot

On top the (absolute) value of the correlation is presented plus the
result of the cor.test as stars. On bottom, the bivariate scatterplots
are plotted with a fitted line. The graph and the code can be found at R
Graph Gallery (137) Correlation Matrix, from where was adapted and
formatted for RKWard.

#### Descriptive Statistics

Currently this allows you to calculate the mean, median, range, and
standard deviation of one or more variables.

#### Means

#### Simple anova

This plug-in allow you to perform a simple anova. Analysis of variance
(ANOVA) is a collection of statistical models and their associated
procedures which compare means by splitting the overall observed
variance into different parts \[Wikipedia encyclopedia\].

#### Skewness and Kurtosis

#### Wilcoxon Tests

#### Ansari-Bradley Tests

### Plots

#### Box Plot

#### Density Plot

#### ECDF Plot

#### Histogram

#### Scatterplot

#### Scatterplot Matrix

Scatterplot matrix plot produces scatterplot matrices with univariate
displays down the diagonal. You can choose among Density plot, Boxplot,
Histogram, QQplot.

#### Stripchart

### Distributions

#### Jarque-Bera Normality Test

Used to test against the normality of a random variable.

### Settings

### Help
