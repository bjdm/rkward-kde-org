---
layout: page
---

# About RKWard

**RKWard is an easy to use and easily extensible IDE/GUI for R.** It aims to combine the power of the R-language with the ease of use of commercial statistics tools.

RKWard's features include:
  - Spreadsheet-like data editor
  - Syntax highlighting, code folding and code completion
  - Data import (e.g. SPSS, Stata and CSV)
  - Plot preview and browsable history
  - R package management
  - Workspace browser
  - GUI dialogs for all kinds of statistics and plots

RKWard's features can be extended by plugins, and it's all free software.

## [Mission Statement](RKWard_Overview.html)

## People

RKWard is developed by a team of volunteers:

### Authors

RKWard is an open project, and there are no clear boundaries between
authors and contributors. However RKWard would not be near its current
state without the work of these people:

  - Thomas Friedrichsmeier
      - Project leader / main developer
  - Pierre Ecochard
      - C++ coder since 0.2.9
  - Stefan Roediger
      - Many plugins, suggestions, marketing, translations
  - Prasenjit Kapat
      - Many plugins, suggestions
  - Meik Michalke
      - Many plugins, suggestions, packaging

### Contributors

Further contributors in alphabetical order:

  - Philippe Grosjean
      - Several helpful comments and discussions
  - Adrien d'Hardemare
      - Plugins and patches
  - Yves Jacolin
      - New website
  - Germán Márquez Mejí­a
      - HP filter plugin, spanish translation
  - Marco Martin
      - A cool icon
  - Daniele Medri
      - RKWard logo, many suggestions, help on wording
  - David Sibai
      - Several valuable comments, hints and patches
  - Ilias Soumpasis
      - Translation, Suggestions, plugins
  - Ralf Tautenhahn
      - Many comments, useful suggestions, and bug reports
  - Jannis Vajen
      - German Translation, bug reports
  - Roland Vollgraf
      - Some patches
  - Roy Qu
      - patches and helpful comments

Many more people on our mailinglists. Sorry, if we forgot to list you.
Please [contact](Contact.html) us to get added.

## License

You are free to use, modify, and distribute RKWard under the terms of
the [GNU General Public
Licence](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html) Version
2 or later. A copy of this license should be included in all downloads.
