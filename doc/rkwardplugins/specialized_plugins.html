<html><head><title>Chapter 10. Concepts for use in specialized plugins</title><link rel="stylesheet" type="text/css" href="../common/kde-default.css"><meta name="generator" content="DocBook XSL Stylesheets V1.79.1"><meta name="keywords" content="KDE, R, rkward, plugins"><link rel="home" href="index.html" title="Introduction to Writing Plugins for RKWard"><link rel="up" href="index.html" title="Introduction to Writing Plugins for RKWard"><link rel="prev" href="include_snippets_vs_embedding.html" title="&lt;include&gt; and &lt;snippets&gt; vs. &lt;embed&gt;"><link rel="next" href="ch10s02.html" title="Previews for data, output and other results"><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta name="GENERATOR" content="KDE XSL Stylesheet V1.13 using libxslt"></head><body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF"><table border="0" cellpadding="0" cellspacing="0" width="100%"><tr class="header"><td colspan="2"> </td></tr><tr id="logo"><td valign="top"><img src="../common/part_of_the_kde_family_horizontal_190.png" alt="Part of the KDE family" width="190" height="68" border="0"></td><td valign="middle" align="center" id="location"><h1>Concepts for use in specialized plugins</h1></td></tr></table><table width="100%" class="header"><tbody><tr><td align="left" class="navLeft" width="33%"><a accesskey="p" href="include_snippets_vs_embedding.html">Prev</a></td><td align="center" class="navCenter" width="34%"> </td><td align="right" class="navRight" width="33%"> 
		      <a accesskey="n" href="ch10s02.html">Next</a></td></tr></tbody></table><div class="chapter"><div class="titlepage"><div><div><h1 class="title"><a name="specialized_plugins"></a>Chapter 10. Concepts for use in specialized plugins</h1></div></div></div><div class="toc"><p><b>Table of Contents</b></p><dl class="toc"><dt><span class="sect1"><a href="specialized_plugins.html#specialized_plugins_plots">Plugins that produce a plot</a></span></dt><dd><dl><dt><span class="sect2"><a href="specialized_plugins.html#rk_graph_on">Drawing a plot to the output window</a></span></dt><dt><span class="sect2"><a href="specialized_plugins.html#preview_plots">Adding preview functionality</a></span></dt><dt><span class="sect2"><a href="specialized_plugins.html#plot_options">Generic plot options</a></span></dt><dt><span class="sect2"><a href="specialized_plugins.html#plot_plugin_example">A canonical example</a></span></dt></dl></dd><dt><span class="sect1"><a href="ch10s02.html">Previews for data, output and other results</a></span></dt><dd><dl><dt><span class="sect2"><a href="ch10s02.html#preview_output">Previews of (HTML) output</a></span></dt><dt><span class="sect2"><a href="ch10s02.html#preview_data">Previews of (imported) data</a></span></dt><dt><span class="sect2"><a href="ch10s02.html#preview_custom">Custom previews</a></span></dt></dl></dd><dt><span class="sect1"><a href="contextualized_plugins.html">Context-dependent plugins</a></span></dt><dd><dl><dt><span class="sect2"><a href="contextualized_plugins.html#context_x11">X11 device context</a></span></dt><dt><span class="sect2"><a href="contextualized_plugins.html#context_import">Import data context</a></span></dt></dl></dd><dt><span class="sect1"><a href="querying_r_for_info.html">Querying <span class="application">R</span> for information</a></span></dt><dt><span class="sect1"><a href="current_object.html">Referencing the current object or current file</a></span></dt><dt><span class="sect1"><a href="optionset.html">Repeating (a set of) options</a></span></dt><dd><dl><dt><span class="sect2"><a href="optionset.html#optionset_driven">"Driven" optionsets</a></span></dt><dt><span class="sect2"><a href="optionset.html#optionset_alternatives">Alternatives: When not to use optionsets</a></span></dt></dl></dd></dl></div><p>
This chapter contains information on some topics that are useful only to certain classes of plugins.
</p><div class="sect1"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a name="specialized_plugins_plots"></a>Plugins that produce a plot</h2></div></div></div><p>
		Creating a plot from a plugin is easy to do. However, there are a few subtle gotchas to avoid, and also some great generic functionality that you should be aware of. This section shows you the basic concepts, and concludes with a canonical example that you should follow whenever creating plot plugins.
	</p><div class="sect2"><div class="titlepage"><div><div><h3 class="title"><a name="rk_graph_on"></a>Drawing a plot to the output window</h3></div></div></div><p>
			In order to draw a plot to the output window, use <code class="function">rk.graph.on()</code> directly before creating the plot, and
			<code class="function">rk.graph.off()</code>, directly afterwards. This is similar to <abbr class="abbrev">e.g.</abbr> calling <code class="function">postscript()</code> and 
			<code class="function">dev.off()</code> in a regular <span class="application">R</span> session.
		</p><p>
			Importantly, however, you must <span class="emphasis"><em>always</em></span> call <code class="function">rk.graph.off()</code> after calling <code class="function">rk.graph.on()</code>. Otherwise the output file will be left in a broken state. To ensure <code class="function">rk.graph.off()</code> really gets called, you should wrap <span class="emphasis"><em>all</em></span> <span class="application">R</span> commands between the two calls in
			<code class="function">try()</code> statement. Never heard of that? Do not worry, it is easy. All you need to do is follow the pattern shown in
			<a class="link" href="specialized_plugins.html#plot_plugin_example" title="A canonical example">example</a>, below.
		</p></div><div class="sect2"><div class="titlepage"><div><div><h3 class="title"><a name="preview_plots"></a>Adding preview functionality</h3></div></div></div><div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><h3 class="title">Note</h3><p>This section discusses adding preview functionality to plugins producing plots. There are separate sections on <a class="link" href="ch10s02.html#preview_output" title="Previews of (HTML) output">previews of (<acronym class="acronym">HTML</acronym>) output</a>, <a class="link" href="ch10s02.html#preview_data" title="Previews of (imported) data">previews of (imported) data</a>, and <a class="link" href="ch10s02.html#preview_custom" title="Custom previews">custom previews</a>. However, it is recommended that you read this section first, as the approach is similar in each case.</p></div><p>
			A very useful feature for all plugins generating a plot/graph is to provide an automatically updating preview. To do so, you will need two things: Adding a <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="command"><span class="command"><strong>&lt;preview&gt;</strong></span></span> check box to your <a class="link" href="mainxml.html" title="Chapter 4. Defining the GUI"><acronym class="acronym">GUI</acronym> definition</a>, and adjusting the <a class="link" href="jstemplate.html" title="Chapter 5. Generating R code from GUI settings">generated code</a> for the preview.
		</p><p>
			Adding a <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="command"><span class="command"><strong>&lt;preview&gt;</strong></span></span> check box is simple. Just place the following somewhere in your <acronym class="acronym">GUI</acronym>. It will take care of all the behind-the-scenes magic of creating a preview device, updating the preview whenever the setting have changed, <abbr class="abbrev">etc.</abbr> Example:
		</p><div class="note" style="margin-left: 0.5in; margin-right: 0.5in;"><h3 class="title">Note</h3><p>Starting with version 0.6.5 of <span class="application">RKWard</span> <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="command"><span class="command"><strong>&lt;preview&gt;</strong></span></span> preview elements are special-cased in plugin dialogs (not wizards): They will be placed in the button-column, irrespective of where exactly they are defined in the UI. It is still a good idea to define them at a sensible place in the layout, for backwards compatibility.
		</p></div><pre class="programlisting">
	&lt;document&gt;
		[...]
		&lt;dialog [...]&gt;
			[...]
			&lt;preview id="preview"/&gt;
			[...]
		&lt;/dialog&gt;
		[...]
	&lt;/document&gt;
		</pre><p>
			And that is it for the <acronym class="acronym">GUI</acronym> definition.
		</p><p>
			Adjusting the JS template is a little more work. You will have to create a new function called <code class="function">preview()</code> in addition to the <code class="function">preprocess()</code>, <code class="function">calculate()</code>, <abbr class="abbrev">etc.</abbr> functions. This function should generate the code needed to produce the plot, and only that. Esp. no printing of headers, <code class="function">rk.graphics.on()</code>, or similar calls. See the <a class="link" href="specialized_plugins.html#plot_plugin_example" title="A canonical example">example</a>, below for the typical pattern that you will use.
		</p></div><div class="sect2"><div class="titlepage"><div><div><h3 class="title"><a name="plot_options"></a>Generic plot options</h3></div></div></div><p>
			You will have noticed that most plotting plugins in <span class="application">RKWard</span> provide a wide range of generic options <abbr class="abbrev">e.g.</abbr> for customizing axis titles or figure margins. Adding these options to your plugin is easy. They are provided by an <a class="link" href="embedding.html" title="Chapter 8. Embedding Plugins into Plugins">embeddable</a> plugin called <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="command"><span class="command"><strong>rkward::plot_options</strong></span></span>. Embed this in your plugin UI like this:
		</p><pre class="programlisting">
	&lt;document&gt;
		[...]
		&lt;logic [...]&gt;
			&lt;connect client="plotoptions.xvar" governor="x.available"/&gt;
			&lt;set id="plotoptions.allow_type" to="true"/&gt;
			&lt;set id="plotoptions.allow_ylim" to="true"/&gt;
			&lt;set id="plotoptions.allow_xlim" to="false"/&gt;
			&lt;set id="plotoptions.allow_log" to="false"/&gt;
			&lt;set id="plotoptions.allow_grid" to="true"/&gt;
		&lt;/logic&gt;
		&lt;dialog [...]&gt;
			[...]
			&lt;embed id="plotoptions" component="rkward::plot_options" as_button="true" label="Plot Options"/&gt;
			[...]
		&lt;/dialog&gt;
		[...]
	&lt;/document&gt;
		</pre><p>
			This will add a button to your UI to bring up a window with plot options. The logic section is just an example. It allows you some control over the plot options plugin. Read more in the plot_options plugin's help page (linked from the help page of any plugin providing the generic options).
		</p><p>
			Next you need to make sure that the code corresponding to your plot options is added to the generated code for your plot. To do so,
			fetch the properties <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="command"><span class="command"><strong>code.preprocess</strong></span></span>, <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="command"><span class="command"><strong>code.printout</strong></span></span>, and <span xmlns:doc="http://nwalsh.com/xsl/documentation/1.0" class="command"><span class="command"><strong>code.calculate</strong></span></span> from the embedded plot options plugin, and insert them into your code as shown in the <a class="link" href="specialized_plugins.html#plot_plugin_example" title="A canonical example">example</a>, below.
		</p></div><div class="sect2"><div class="titlepage"><div><div><h3 class="title"><a name="plot_plugin_example"></a>A canonical example</h3></div></div></div><p>
			Here is an example .JS file that you should use as a template, whenever you create a plotting plugin:
		</p><pre class="programlisting">
  function preprocess () {
    // the "somepackage" is needed to create the plot
    echo ("require (somepackage)\n");
  }

  function preview () {
    // we call all stages of the general code. Only the printout () function needs to be called slightly different for the plot preview
    preprocess ();
    // calculate (); // in this example, the plugin has no calculate () function.
    printout (true); // in this case, 'true' means: Create the plot, but not any headers or other output.
  }
  
  function printout (is_preview) {
    // If "is_preview" is set to false, it generates the full code, including headers.
    // If "is_preview" is set to true, only the essentials will be generated.

    if (!is_preview) {
      echo ('rk.header (' + i18n ("An example plot") + ')\n\n');
      echo ('rk.graph.on ()\n');
    }
    // only the following section will be generated for is_preview==true

    // remember: everything between rk.graph.on() and rk.graph.off() should be wrapped inside a try() statement:
    echo ('try ({\n');
    // insert any option-setting code that should be run before the actual plotting commands.
    // The code itself is provided by the embedded plot options plugin. printIndentedUnlessEmpty() takes care of pretty formatting.
    printIndentedUnlessEmpty ('\t', getString ("plotoptions.code.preprocess"), '', '\n');

    // create the actual plot. plotoptions.code.printout provides the part of the generic plot options
    // that have to be added to the plotting call, itself.
    echo ('plot (5, 5' + getString ("plotoptions.code.printout") + ')\n');

    // insert any option-setting code that should be run after the actual plot.
    printIndentedUnlessEmpty ('\t', getString ("plotoptions.code.calculate"), '\n');
    echo ('})'\n);  // the closure of the try() statement

    if (!is_preview) {
      echo ('rk.graph.off ()\n');
    }
  }
		</pre></div></div></div><table width="100%" class="bottom-nav"><tr><td width="33%" align="left" valign="top" class="navLeft"><a href="include_snippets_vs_embedding.html">Prev</a></td><td width="34%" align="center" valign="top" class="navCenter"><a href="index.html">Contents</a></td><td width="33%" align="right" valign="top" class="navRight"><a href="ch10s02.html">Next</a></td></tr><tr><td width="33%" align="left" class="navLeft">&lt;include&gt; and &lt;snippets&gt; vs. &lt;embed&gt; </td><td width="34%" align="center" class="navCenter"><a href="index.html">Up</a></td><td width="33%" align="right" class="navRight"> Previews for data, output and other results</td></tr></table></body></html>