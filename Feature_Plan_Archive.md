---
layout: page
---

# Feature plan archive

* Table of contents
{:toc}

## Features implemented in RKWard 0.7.4

### More import options

**Description**: Better support for importing data, including a generic rio-based importer, better support for pasting in data, and an import assistant.

**Points for discussion / issues**:

**Development status**: <span style="color:green">Done</span>.

**Testing status**: N/A


### Preview for (almost) all plugins

**Description**: All applicable plugins in the main distribution now provide a preview, allowing to view results faster, and helping to keep the output clean.

**Points for discussion / issues**:

**Development status**: <span style="color:green">Done</span>.

**Testing status**: N/A


### Fewer pop-up dialogs during package installation

**Description**: Especially the initial start of RKWard is crowded with pop-up dialogs. Reworking the package installation dialog takes another two of these,
without loss of functionality.

**Points for discussion / issues**:

**Development status**: <span style="color:green">Done</span>.

**Testing status**: N/A


### Rework "Welcome to RKWard" page

**Description**: Should be more dashboard like, with little text, but useful links

**Points for discussion / issues**: https://mail.kde.org/pipermail/rkward-devel/2022-April/005689.html
Replaces startup dialog

**Development status**: <span style="color:green">Done</span>.

**Testing status**: N/A

## Features implemented in RKWard 0.7.3

### RKWard Output as document

**Description**: The RKWard output file behaves more like a regular document: Changes can be saved or discarded,
stored as a single file (internally a .zip archive).

**Points for discussion / issues**:

**Development status**: <span style="color:green">Done</span>.

**Testing status**: N/A

### Implement R 4.1 and R 4.2 graphics features

**Description**: gradients, patterns, clip paths, masks, stroked and filled paths, groups, lumincance masks

**Points for discussion / issues**:

**Development status**: <span style="color:green">Done</span>.

**Testing status**: N/A

### Integrated dialog to ask about all changed files on exit

**Description**: Instead of asking separately for each changed script

**Points for discussion / issues**: Unfortunately, R workspace is still detected as changed, every time.

**Development status**: <span style="color:green">Done</span>.

**Testing status**: N/A


## Features implemented in RKWard 0.7.2

### "First run wizard" or check installation feature

**Description**: To check for - and if feasible, correct - common setup problems.

**Points for discussion / issues**:
Run for each new version. No way to install external software, yet.

**Development status**: <span style="color:green">Done</span>.

**Testing status**: N/A


### Integration of kate plugins

**Description**: Many useful plugins, e.g. search in files, are available from kate. RKWard should be able to embed these.

**Points for discussion / issues**:
Not all kate plugins make sense in the context of RKWard, and for some others, the way the UI is merged is not yet ideal.

**Development status**: <span style="color:green">Done</span>.

**Testing status**: N/A

### Bring enhanced code hinting features to the R Console window

**Description**: Script editor windows have much improved code hinting abilities, now. Merge these into the R Console window.

**Points for discussion / issues**:
I'm not quite happy with the behavior e.g. when typing "?print<Enter>", yet (requires <Enter> twice)

**Development status**: <span style="color:green">Done</span>.

**Testing status**: N/A

## Features implemented in RKWard 0.7.1

### Script previews, including R markdown

**Description**: The script editor has gained live previews. This can be used to view the
rendering of an R Markdown (.Rmd) script, comfortably, while typing. But also, you can preeview scripted plots, RKWard output, or R Console output while developing.

**Points for discussion / issues**:

**Development status**: <span style="color:green">Done</span>.

**Testing status**: N/A

### Better integration with R installations

**Description**: By default, RKWard will now offer to install R packages to the same user library location as a regular R session, making it easier to switch back and forth between RKWard and a plain R command line session. Further, RKWard's own R package no longer needs to be installed during build-time, but will be installed in a dedicated location as needed. This makes it easier to use RKWard with several separate installations of R (provided, they are binary compatible; different patch releases will generally work), and should help avoid
related installation problems.

**Points for discussion / issues**:
The R_LIBDIR cmake parameter is made obsolete by this change.

**Development status**: <span style="color:green">Done</span>.

**Testing status**: N/A

## Features implemented in RKWard 0.7.0

### Split views

**Description**: Allow the main window to be split horizontally or
vertically, to allow side-by-side view of the same or different scripts
and data.

**Points for discussion / issues**:

  - Wishlist ticket: <https://bugs.kde.org/show_bug.cgi?id=380599>

**Development status**: <span style="color:green">Done</span>.

**Testing status**: N/A

## Features implemented in RKWard 0.6.5

### Generalized previews

**Description**: Allow preview-types other than plots. Importantly,
data, and HTML. Previews are now shown attached to the dialog, by
default.

**Points for discussion / issues**:

  - <https://mail.kde.org/pipermail/rkward-devel/2015-December/004391.html>

**Development status**: <span style="color:green">Done</span>.

**Testing status**: N/A

## Features implemented in RKWard 0.6.4

### Usability improvements

**Description**: Importantly, the workspace browser and the <optionset>
element are targetted.

**Points for discussion / issues**:

  - <https://mail.kde.org/pipermail/rkward-devel/2015-November/004287.html>
  - <https://mail.kde.org/pipermail/rkward-devel/2015-November/004283.html>

**Development status**: <span style="color:green">Done</span>.

**Testing status**: N/A

## Features implemented in RKWard 0.6.3

### 100% translatability

**Description**: Allow plugins and help pages to be translated.

**Points for discussion / issues**:

  - See <https://rkward.kde.org/doc/rkwardplugins/i18n.html>

**Development status**: <span style="color:green">Done. Many
translations already exist.</span>.

**Testing status**: N/A

### Better plugin customization

**Description**: Allow external plugins to override / update installed
plugins. Allow to selectively hide specific plugins.

**Points for discussion / issues**:

**Development status**: <span style="color:green">Done - for now. No UI
interface for by-plugin customization, yet.</span>.

**Testing status**: N/A

## Features implemented in RKWard 0.6.2

### Make graphics device work on Mac OS X

**Description**: Make graphics device work on Mac OS X

**Points for discussion / issues**:

  - Several variants are possible:
      - Based on the quartz device, somehow syncing graphs to a draw
        area in the frontend
      - Cross-platform solution. Implement our own graphics device,
        after all, which will forward all graphics calls to a
        QGraphicsScene in the frontend. Could borrow some code from
        qtutils package.
          - Cons: Implementation / maintainance effort, performance?.
          - Pros: Cross-platform, performance?, will easily be made to
            work over the network, more reliable hooks for history
            functions.

**Development status**: <span style="color:green">Implemented as the new
RK() default graphics device (cross-platform).</span>.

**Testing status**: N/A

### Allow plugin UI script code to query R for information

**Description**: Allow plugin UI script code to query R for information

**Points for discussion / issues**:

  - There are a bunch of associated caveats. In general, the recommended
    usage is via the level_select embeddable plugin.
  - See documenation on doRCommand()

**Development status**: <span style="color:green">Ready for use.</span>.

**Testing status**: N/A

## Features implemented in RKWard 0.6.1

### Simple matrix editor for use in plugins

**Description**: Provide a simple editor for (small) matrices for
embedding in plugin dialogs.

**Points for discussion / issues**:

  - Should allow on-the-fly resizing
  - Strings and numbers, probably no factors, no support for invalid
    values
  - Called <matrix>

**Development status**: <span style="color:green">Ready for use</span>.

**Testing status**: N/A

### Sets of repeated options in plugins

**Description**: Provide the possibility to repeat certain options for
an arbitrary number of items. E.g. line style and color for a number of
variables selected for plotting

**Points for discussion / issues**:

  - Called <optionset>

**Development status**: <span style="color:green">Ready for use</span>.

**Testing status**: N/A

### Plugin for simple filtering of data(.frames)

**Description**: A plugin that can be used to filter data according to
certain criteria.

**Points for discussion / issues**:

  - Should support filtering for single fixed values or by formula
  - Ideally, support filtering for an arbitrary number of criteria in
    one step. This would require a working RKOptionset.
  - By default, should filter to an object named *oldname*.filtered.
  - Should support storing a filter expression in the unfiltered set,
    for later re-use.

**Development status**: <span style="color:yellow">Initial simple
implementation is part of the preview release</span>.

**Testing status**: N/A

## Features implemented in RKWard 0.6.0

### RKWard on the Mac

**Description**: Provide a ready-to-use workflow for compiling &
installing RKWard on the Mac, including easy to install binaries.

**Points for discussion / issues**:

  - Embedding on-screen graphics is not supported, initially

**Development status**: <span style="color:green">Ready for use.</span>.

**Testing status**: N/A

### Debugger support

**Description**: Detect when the backend is in a debugger context, and
provide a more useful console in this case. Also a simple frame
inspector.

**Points for discussion / issues**:

  - Debugger console should gain highlighting, eventually.
  - Watched expressions to be added in a later release.

**Development status**: <span style="color:green">Ready for use.</span>.

**Testing status**: N/A

## Features implemented in RKWard 0.5.7

### Simplification of plugin-generated code

**Description**: Many plugins, which support running an operation (e.g.
a test) on several objects in one go, generate fairly complex code,
involving substitute() and eval(). Simplify these to use rk.list(),
instead.

**Points for discussion / issues**:

  - Intermediate goal is to change only the generated code, but *not*
    the output. Existing automated tests will help with controlling
    this.

**Development status**: <span style="color:green">Ready for use.</span>.

**Testing status**: N/A

### Proper fix for printing support

**Description**: Printing of graphics is sort of broken, since kprinter
was never ported to KDE 4. Fix this by providing some auto-magic, which
will pick the best available alternative out of the installed programs.

**Points for discussion / issues**:

  - Uses new pseudo device: rk.printer.device()
  - Tries to load okular part for print preview, and falls back to any
    registered external postscript viewer

**Development status**: <span style="color:green">Ready for use.</span>.

**Testing status**: N/A

## Features implemented in RKWard 0.5.6

### Global R interrupt button

**Description**: Simply a button that will cancel any current R
operations.

**Points for discussion / issues**:

  - Must not cancel internal "Sync" commands.
  - When done, the "Pending jobs" tool window can be hidden somewhere,
    as it will rarely still be used.

**Development status**: <span style="color:green">Ready for use</span>.

**Testing status**: N/A

### Transcript of console to output

**Description**: Add an option to copy all console input / output to the
output window.

**Points for discussion / issues**:

**Development status**: <span style="color:green">Ready for use</span>.

**Testing status**: N/A

## Features implemented in RKWard 0.5.5

### Separate frontend and backend into different processes

**Description**: Make the backend run inside a separate process, in
order to enhance stability, and compatibility with Java/Gtk based
packages.

**Points for discussion / issues**:

  - Currently, the "old" solution, of the backend running in a separate
    thread of the same process can be enabled at compile time, by adding
    -DRKWARD_THREADED=1 to the cmake options.

**Development status**: <span style="color:green">Ready for use</span>.

**Testing status**: N/A

## Features implemented in RKWard 0.5.4

### Plot history

**Description**: Automatically record plots in on-screen devices, and
offer navigation features.

**Points for discussion / issues**:

  - See
    <http://www.mail-archive.com/rkward-devel@lists.sourceforge.net/msg00890.html>
  - See
    <http://www.mail-archive.com/rkward-devel@lists.sourceforge.net/msg00931.html>
  - See
    <http://www.mail-archive.com/rkward-devel@lists.sourceforge.net/msg00955.html>

**Development status**: <span style="color:green">Ready for testing /
use</span>.

**Testing status**: Automated test exists.

### Support rownames in data.frame editor

**Description**: The data.frame editor does not yet support row.names.
We should add support for that.

**Points for discussion / issues**:

  - Make sure to reset rownames to the default (1:n) when custom row
    names are not / no longer used.
  - Currently rownames are fully discarded when inserting / deleting
    rows. Obviously that needs fixing, too.

**Development status**: <span style="color:green">Ready for use</span>.

**Testing status**: N/A

### Initial support for scripted plugin logic

**Description**: A script based (easier, and more flexible) approach to
handling the GUI logic in plugins.

**Points for discussion / issues**:

  - [Converting Plugins from PHP to
    JS\#GUI_logic](Converting_Plugins_from_PHP_to_JS#GUI_logic.html)

**Development status**: <span style="color:green">Ready for testing /
feedback</span>.

**Testing status**: N/A

### Support for editor-related plugins

**Description** Plugins working on data.frames should be available for
the context of the data editor. E.g. sorting, data transformations, etc.

**Points for discussion / issues**:

  - Need to create a new component context for this (trivial)
  - Save-object chooser needs to accept parent objects one way or
    another. What should the GUI look like?

**Development status**: <span style="color:green">Ready for testing /
feedback</span>.

**Testing status**: N/A

## Features implemented in RKWard 0.5.3

### New Scripting Solution

**Description**: The PHP backend just doesn't cut it any longer. Too
heavy a dependency, too slow on windows, too inflexible. We'll probably
use QtScript based on the Kross framework, next. Goal for this release
is to replace the current functionality of the PHP backend. In later
releases, new features will be added, esp. the ability to use scripting
for the GUI logic, and the ability to script custom widgets.

**Points for discussion / issues**:

  - <http://www.mail-archive.com/rkward-devel@lists.sourceforge.net/msg00405.html>
  - Depends on [\#Automated Plugin
    Testing](#Automated_Plugin_Testing.html)
  - [Converting Plugins from PHP to
    JS](Converting_Plugins_from_PHP_to_JS.html)

**Development status**: <span style="color:green">QtScript backend
implemented, all official plugins ported</span>.

**Testing status**: N/A

## Features implemented in RKWard 0.5.2

### Configurable toolbars

**Description**: Toolbars are now configurable as well. Settings -\>
Configure Toolbars

**Points for discussion / issues**:

  - Only the currently active actions can be configured at a time.

**Development status**: <span style="color:green">**Ready for testing /
feedback**</span>. Implemented on the KDE 4 branch, no backport planned.

**Testing status**: ???

-----

### Automated Plugin Testing

**Description**: Write an R script that invokes a plugin with various
different settings, checks whether the output is still the same as in
earlier runs of the test, checks for any errors, checks for properties
of created R objects (if applicable) etc. This script could be run in
regular intervals to detect any possible problems due to new version of
R/packages, changes in RKWard, etc. very quickly.

In the long term, **all** plugins should have tests in the suite.

**Points for discussion / issues**:

  - [Automated Plugin Testing](Automated_Plugin_Testing.html)

**Development status**: <span style="color:green">Ready for use</span>.
All directly callable plugins have at least one test.

**Testing status**: N/A

## Features implemented in RKWard 0.5.1

### "Run again"-link

**Description**: Each plugin (with the exception of plugins that only
work in special contexts, such as e.g. adding a grid to an X11-device)
gets a "Run again" link added to its output. Clicking this link will
bring up the plugin again with the same settings already filled in. That
way it becomes easily possible to redo an action with slight
modifications. This probably makes plugins an order of magnitude more
useful.

No changes should be needed in the plugins themselves.

**Points for discussion / Issues**:

  - Is "Run again" a good label for this link?
  - Most/all plugins should print an rk.header() in the output, so you
    know, what the run again link is all about
      - Which ones do not produce a header, yet?
      - Which ones should not? Perhaps the setwd-plugin, for instance?
      - Ideally the header should also be written in case the generated
        code produces an error. This is not currently the case.

**Development Status**: <span style="color:green">**Ready for testing /
feedback**</span>. Implemented on the KDE 4 branch, no backport planned.

**Testing Status**: ???

-----

### Plugins can be invoked from R code

**Description**: Use e.g. rk.call.plugin ("rkward::t_test_two_vars",
x.available="x", y.available="y", submit.mode="auto") to fire up the
t-test plugin with objects x and y preselected (if possible) and
automatically submit the code, if possible. The main idea behind this is
that we can use this for [\#Automated Plugin
Testing](#Automated_Plugin_Testing.html).

**Points for discussion / issues**: Naming of parameters should probably
be revised.

**Development status**: <span style="color:green">**Ready for testing /
feedback**</span>. Implemented on the KDE 4 branch, no backport planned.

**Testing status**: ???

-----

### PHP function makeHeaderCode () for use in Plugins

**Description**: A new PHP convenience function is available for use in
plugins. makeHeaderCode() basically just produces the
rk.header()-syntax. The advantages are

  - Easier typing / reading, as we don't need to jump in and out of PHP
    code some much
  - More correct quoting (important for the rare case that parameters
    can contain quotes).

**Points for discussion / issues**:

  - Many tests have some unquoted parameters in the call to rk.header(),
    namely rk.describe.alternative(). There should be a good way to
    support this case as well.
  - Ideally, most plugins should produce an rk.header() of some sort.
    Ones which don't, yet are:
      - TODO: find out which, and list them, here
  - It may or may not be worth while to use makeHeaderCode() in existing
    plugins, now. Probably it should wait for [\#Automated Plugin
    Testing](#Automated_Plugin_Testing.html)

**Development status**: <span style="color:green">**Ready for testing /
feedback**</span>. Implemented on the KDE 4 branch,
<span style="color:red">needs backporting</span>.
<span style="color:red">Needs documentation</span>.

**Testing status**: ???

-----

### Action to set WD to dir of current script

**Description**:
<http://sourceforge.net/tracker/?func=detail&aid=1705553&group_id=50231&atid=459010>

**Points for discussion / issues**: A good label for this?

**Development status**: <span style="color:green">**Ready for testing /
feedback**</span>. Implemented on the KDE 4 branch, no backport planned.

**Testing status**: ???

-----

### Configurable keyboard shortcuts

**Description**: Keyboard shortcuts for (almost) all actions are
*finally* configurable. Settings -\> Configure Keybindings

**Points for discussion / issues**:

  - Only the currently active actions can be configured at a time.
  - Potential conflicts between application global and currently
    non-active action-shortcuts will not be detected.

**Development status**: <span style="color:green">**Ready for testing /
feedback**</span>. Implemented on the KDE 4 branch, no backport planned.

**Testing status**: ???

-----

### Windows Port

**Description**: Port RKWard to MS Windows.

**Points for discussion / issues**: Outline of main TODOs, left:

  - Polish the installer
      - simplify, if possible
      - test
  - Plugins
      - The PHP backend performance is lousy on windows. Find a solution
        for that
  - Testing
  - Documentation [RKWard_on_Windows](RKWard_on_Windows.html)

**Development status**:

  - <span style="color:yellow">First technical preview release will be
    announced, soon</span>.

**Testing status**: N/A.
