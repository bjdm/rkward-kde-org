---
layout: page
---

# Development status of plugins

Keep track of the development status of Plugins, to make sure they get
decent testing before entering a distribution. **This page is not currently used. Information provided, here, is outdated!**

## Plugins under planning / wishlist

Refer to [Plugins_under_planning](Plugins_under_planning.html).

## New / reworked plugins

| ID / Name                    | Pluginmap      | Author(s)   | Development status | First released in | Code correctness review | Style review | Result validation | Help review | Automated test(s) | References                                                                        |
| ---------------------------- | -------------- | ----------- | ------------------ | ----------------- | ----------------------- | ------------ | ----------------- | ----------- | ----------------- | --------------------------------------------------------------------------------- |
| import_stata / Import Stata | import_export | Michael Ash | Complete           | 0.5.3             |                         |              |                   |             | yes               | [1](http://www.mail-archive.com/rkward-devel@lists.sourceforge.net/msg00512.html) |

Plugin development / review status

Some explanations on the fields in above table:

  - *Development status*: Generally one of "under development" (red),
    "functional" (yellow), and "complete" (green).
  - *Code correctness review*: Please add your signature in this field,
    if you have verified the correctness of the R code generated. Color
    codes: No review: red, one review: yellow, two or more reviews:
    green.
  - *Style review*: Style of php file and generated R code (readability,
    comments, indentation), xml file (indentation, no superfluous
    attributes), help file.
  - *Result validation*: Validation of the actual *result* of the
    plugin, preferably by cross-validating with a different statistics
    tool. Sign when you have validated this, preferably with a short
    comment on how you validated it.
  - *Help review*: Review of the help file (if provided): Readability,
    completeness.
  - *Automated test(s)*: Is at least one automated test available?
  - *References*: Links to discussion on the mailing list or other
    relevant resources.

## Plugins already included in a release

| ID / Name                            | Pluginmap | Author(s)                                 | Development status | First released in | Code Correctness review                                     | Style review                                                | Result validation | Help review                                                 | Automated test(s) | References                                                                                                                                                          |
| ------------------------------------ | --------- | ----------------------------------------- | ------------------ | ----------------- | ----------------------------------------------------------- | ----------------------------------------------------------- | ----------------- | ----------------------------------------------------------- | ----------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| hp_filter / Hodrick-Prescott filter | analysis  | Manchito | Complete           | 0.5.0b            | Tfry 09:52, 23 September 2009 (UTC) | Tfry 09:52, 23 September 2009 (UTC) |                   | Tfry 09:52, 23 September 2009 (UTC) | yes               | [2](http://www.mail-archive.com/rkward-devel@lists.sourceforge.net/msg00109.html) [3](http://www.mail-archive.com/rkward-devel@lists.sourceforge.net/msg00111.html) |
| levene_test / Levene's test         | analysis  | Sjar     | Complete           | 0.4.9             | Tfry 09:52, 23 September 2009 (UTC) | Tfry 09:52, 23 September 2009 (UTC) |                   | Tfry 09:52, 23 September 2009 (UTC) | yes               |                                                                                                                                                                     |

Plugin development / review status

[Plugins included in 0.4.8](NewPlugins0_4_8.html)

[Plugins included in 0.4.7](NewPlugins0_4_7.html)

[Plugins included in 0.4.6](NewPlugins0_4_6.html)
