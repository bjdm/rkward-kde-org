---
layout: page
---

# Plugins introdoced or reworked in RKWard 0.4.6

### Import / Export

  - SPSS import
      - Status: Complete
      - Tested by (when): tfry / 2007-02-01
      - Responsible: tfry

<!-- end list -->

  - CVS import
      - Status: Complete
      - Tested by (when): tfry / 2007-02-01
      - Responsible: tfry

### X11

  - Export X11 Device
      - Status: Complete
      - Tested by (when): tfry / 2007-01-31
      - Responsible: tfry

### Analysis (tests)

  - Wilcoxon Test
      - Status: Complete
      - Tested by (when): tfry / 2007-02-08
      - Responsible: sjar

<!-- end list -->

  - Wilcoxon Exact Test
      - Status: Complete (no docs, yet)
      - Tested by (when): tfry / 2007-02-08
      - Responsible: sjar

<!-- end list -->

  - Ansari-Bradley two-sample test
      - Status: Complete
      - Tested by (when): tfry / 2007-02-08
      - Responsible: sjar

<!-- end list -->

  - Ansari-Bradley two-sample exact test
      - Status: Complete (no docs, yet)
      - Tested by (when): tfry / 2007-02-08
      - Responsible: sjar

### Plots

  - Barplot
      - Status: Complete
      - Tested by (when): tfry / 2007-02-07
      - Responsible: nono_231

<!-- end list -->

  - Scatterplot Matrix
      - Status: Complete
      - Responsible: nono_231

<!-- end list -->

  - Correlation Matrix Plot
      - Status: Complete
      - Responsible: nono_231

### Distribution plots

  - Plot Beta distribution
      - Status: Complete
      - Tested by (when): pk / 2007-02-06
      - Tested by (when): tfry / 2007-02-07
      - Responsible: pk

<!-- end list -->

  - Plot Binomial distribution
      - Status: Complete
      - Tested by (when): pk / 2007-02-06
      - Tested by (when): tfry / 2007-02-07
      - Responsible: pk

<!-- end list -->

  - Plot Cauchy distribution
      - Tested by (when): tfry / 2007-02-07
      - Responsible: pk

<!-- end list -->

  - Plot Exponential distribution
      - Tested by (when): tfry / 2007-02-07
      - Responsible: pk

<!-- end list -->

  - Plot Gamma distribution
      - Tested by (when): tfry / 2007-02-07
      - Responsible: pk

<!-- end list -->

  - Plot Geometric distribution
      - Tested by (when): tfry / 2007-02-07
      - Responsible: pk

<!-- end list -->

  - Plot Hypergeometric distribution
      - Tested by (when): tfry / 2007-02-07
      - Responsible: pk

<!-- end list -->

  - Plot Logistic distribution
      - Tested by (when): tfry / 2007-02-07
      - Responsible: pk

<!-- end list -->

  - Plot Lognormal distribution
      - Tested by (when): tfry / 2007-02-07
      - Responsible: pk

<!-- end list -->

  - Plot Negative Binomial distribution
      - Tested by (when): tfry / 2007-02-07
      - Responsible: pk

<!-- end list -->

  - Plot Studentized Range (Tukey) distribution
      - Tested by (when): tfry / 2007-02-07
      - Responsible: pk

<!-- end list -->

  - Plot Uniform distribution
      - Tested by (when): tfry / 2007-02-07
      - Responsible: pk

<!-- end list -->

  - Plot Weibull distribution
      - Tested by (when): tfry / 2007-02-07
      - Responsible: pk

<!-- end list -->

  - Plot Wilcoxon Rank Sum distribution
      - Tested by (when): tfry / 2007-02-07
      - Responsible: pk
