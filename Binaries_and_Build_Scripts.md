---
layout: page
---

# Installing RKWard on Linux/Unix: Binaries and build instructions

This page provides links to the relevant information pages for the various distributions for which official or inofficial packages are available.

If your distribution is not listed, or does not provide an up-to-date-package, consider [building RKWard from source](Building_RKWard_From_Source.html),
and/or filing a support ticket against your distribution.

* 
{:toc}


## Debian

RKWard package in debian: <https://packages.debian.org/sid/rkward>

## Ubuntu

RKWard package in debian: <https://packages.ubuntu.com/search?keywords=rkward&searchon=names&suite=all&section=all>

### Project-provided Ubuntu repositories

We provide a variety of automated builds for Ubuntu on our
[Launchpad project page](https://launchpad.net/rkward):

  - If you are using the version R shipped with Ubuntu by default:
      - The latest stable release of RKWard:
        <https://launchpad.net/~rkward-devel/+archive/rkward-stable>
      - Testing snapshots:
        <https://launchpad.net/~rkward-devel/+archive/rkward-devel>
      - Daily development snapshots:
        <https://launchpad.net/~rkward-devel/+archive/rkward-kf5-dailys>
  - If you are using the CRAN version of R:
      - The latest stable release of RKWard:
        <https://launchpad.net/~rkward-devel/+archive/rkward-stable-cran>
      - Testing snapshots:
        <https://launchpad.net/~rkward-devel/+archive/rkward-devel-cran>
      - Daily development snapshots:
        <https://launchpad.net/~rkward-devel/+archive/rkward-kf5-dailys-cran>

## Gentoo

RKWard package in Gentoo:
<https://packages.gentoo.org/package/sci-mathematics/rkward>

## FreeBSD

Information on the FreeBSD port of RKWard is here: <https://www.freshports.org/math/rkward-kde/>

## SUSE / openSUSE

<https://software.opensuse.org/package/rkward5>

## Fedora

<https://src.fedoraproject.org/rpms/rkward>

## ArchLinux

- Stable release: <https://archlinux.org/packages/community/x86_64/rkward/>

## Windows

See [RKWard on Windows](RKWard_on_Windows.html) for details.

## Mac OS

See [RKWard on Mac](RKWard_on_Mac.html) for details.
