---
layout: page
---

## Purpose of this page

To give a very rough idea on the development status of the major
components of RKWard. The table below is really mostly an estimation of
how much work is still to come, and which components need the most
attention.

## Estimated state of version 0.4.7

<table id="status" style="margin-right: 20px;">

<tbody>

<tr>

<td class="header" width="30%">

Component

</td>

<td class="header" rowspan="1" colspan="3" width="15%">

Proof of concept

</td>

<td class="header" rowspan="1" colspan="3" width="15%">

Alpha

</td>

<td class="header" rowspan="1" colspan="3" width="15%">

Beta

</td>

<td class="header" rowspan="1" colspan="3" width="15%">

Stable

</td>

<td class="header" rowspan="1" colspan="2" width="10%">

Complete

</td>

</tr>

<tr>

<td class="HeaderRow">

Application framework

</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ffff66">



</td>

<td bgcolor="#ffff66">



</td>

<td bgcolor="#ffff66">



</td>

<td bgcolor="#ccffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td>



</td>

<td>



</td>

</tr>

<tr>

<td class="HeaderRow">

internal R-interface

</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ffff66">



</td>

<td bgcolor="#ffff66">



</td>

<td bgcolor="#ffff66">



</td>

<td bgcolor="#ccffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td>



</td>

<td>



</td>

</tr>

<tr>

<td class="HeaderRow">

Script files editor

</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ffff66">



</td>

<td bgcolor="#ffff66">



</td>

<td bgcolor="#ffff66">



</td>

<td bgcolor="#66ff66">



</td>

<td bgcolor="#ccffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td>



</td>

<td>



</td>

</tr>

<tr>

<td class="HeaderRow">

Console

</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ffff66">



</td>

<td bgcolor="#ffff66">



</td>

<td bgcolor="#ffffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td>



</td>

<td>



</td>

</tr>

<tr>

<td class="HeaderRow">

Pending jobs control panel

</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ffffcc">



</td>

<td bgcolor="#ffffcc">



</td>

<td bgcolor="#ffffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td>



</td>

<td>



</td>

</tr>

<tr>

<td class="HeaderRow">

Plugin framework

</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ffff66">



</td>

<td bgcolor="#ffff66">



</td>

<td bgcolor="#ffff66">



</td>

<td bgcolor="#ccffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td>



</td>

<td>



</td>

</tr>

<tr>

<td class="HeaderRow">

Plugins (read: actual functionality)

</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ffff66">



</td>

<td bgcolor="#ffffcc">



</td>

<td bgcolor="#ffffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td>



</td>

<td>



</td>

</tr>

<tr>

<td class="HeaderRow">

Data-editor

</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ffffcc">



</td>

<td bgcolor="#ffffcc">



</td>

<td bgcolor="#ffffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td>



</td>

<td>



</td>

</tr>

<tr>

<td class="HeaderRow">

Output formatting

</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ffcccc">



</td>

<td bgcolor="#ffffcc">



</td>

<td bgcolor="#ffffcc">



</td>

<td bgcolor="#ffffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td>



</td>

<td>



</td>

</tr>

<tr>

<td class="HeaderRow">

Help framework

</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ffff66">



</td>

<td bgcolor="#ffffcc">



</td>

<td bgcolor="#ffffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td>



</td>

<td>



</td>

</tr>

<tr>

<td class="HeaderRow">

Help

</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#666666">



</td>

<td bgcolor="#ff6666">



</td>

<td bgcolor="#ffcccc">



</td>

<td bgcolor="#ffcccc">



</td>

<td bgcolor="#ffffcc">



</td>

<td bgcolor="#ffffcc">



</td>

<td bgcolor="#ffffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td>



</td>

<td>



</td>

</tr>

<tr>

<td class="HeaderRow">

Office integration

</td>

<td bgcolor="#cccccc">



</td>

<td bgcolor="#cccccc">



</td>

<td bgcolor="#cccccc">



</td>

<td bgcolor="#ffcccc">



</td>

<td bgcolor="#ffcccc">



</td>

<td bgcolor="#ffcccc">



</td>

<td bgcolor="#ffffcc">



</td>

<td bgcolor="#ffffcc">



</td>

<td bgcolor="#ffffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td bgcolor="#ccffcc">



</td>

<td>



</td>

<td>



</td>

</tr>

</tbody>

</table>

## More detailed information

[RKWard Overview](RKWard_Overview.html)

[List Of Menu Items and
Plugins](List_Of_Menu_Items_and_Plugins.html)

## What can I do to help?

[Open Tasks](Open_Tasks.html)

[Contact](Contact.html)

[Category:Developer
Information](Category:Developer_Information.html)