---
layout: page
---

# General FAQ

* Table of contents
{:toc}

## I need help\! Where can I ask?

In case you can't find the relevant information in this wiki, see [this page](Contact.html) for the main ways to contact us.

## Overview

### What is RKWard?

RKWard is meant to become an easy to use, transparent frontend to the
[R-language](http://r-project.org). R is a very powerful, yet
hard-to-get-into scripting-language with a strong focus on statistical
functions. RKWard makes R programming easier and faster, by providing a
graphical frontend that can be used by unexperienced users (thanks to
easy-to-use dialog boxes) as well as experts (which are attracted by the
integrated development environement proposed by RKWard).

RKWard then is (will be) something like a free replacement for
commercial statistical packages. In addition to ease of use, three
aspects are particularily important:

  - It's a transparent interface to the underlying R-language. That is,
    it does not hide the powerful syntax, but merely provides a
    convenient way, in which both newbies and R-experts can accomplish
    most of their tasks. A GUI can never provide an interface to the
    whole power of a language like R. In some cases users will want to
    tweak some functions to their particular needs and esp. to automate
    some tasks. By making the "inner workings" visible to the user,
    RKWard makes it easy for the user to see where and how to use
    R-syntax to accomplish their goals.
  - For the output, RKWard strives to separate content and design to a
    high degree. It will not try to design its own tables/graphs, etc,
    which have to be converted to the style used in the rest of a
    publication by hand. Currently RKWard uses HTML for its output.
    Using appropriate style definitions reformatting this output to
    match the rest of the publication will be easily doable. In future
    releases RKWard will even seek stronger integration with existing
    office suites.
  - it relies on a language, that is not only very powerful, but also
    extensible, and for which dozens of extensions already exist.

**And of course, it's free as in free speech\!**

### Are there other R GUIs?

Yes, there are. Here's the most complete list to date:
<http://www.sciviews.org/_rgui/> . The [R Wiki](http://wiki.r-project.org/rwiki/doku.php) has an overview, too
(still lacking information at the time of this writing):
<http://wiki.r-project.org/rwiki/doku.php?id=guis:projects> .

Some popular choices are:

  - [Sciviews](http://sciviews.org) (Windows, only).
  - [RCommander](http://socserv.mcmaster.ca/jfox/Misc/Rcmdr/) by John
    Fox might be mentioned as another versatile, but somewhat limited
    R-GUI, build upon TCL/TK and R language. RCommander is available for
    Windows and U-/Linux systems alike.
  - Another option of a GUI-like approach is offered by
    [Emacs/ESS](http://ess.r-project.org/Manual/ess.html) which comes
    with powerful capabilities to master programming, debugging and
    profiling of self written GNU R functions. That said, don't expect
    Emacs/ESS to be kind of a conventional GUI like SPSS or SAS (which
    by the way can also be run in Emacs/ESS\!) rather than a terminal
    like
    application offering lots of R functionality.

## Platforms

### Linux / Unix

#### Is RKWard available for Linux / Unix?

Yes, it is\!

#### How can I install RKWard on Linux / Unix?

Many Linux distributions include RKWard today. Check your distributions
package manager, or see [the list of known binaries and build scripts](Binaries_and_Build_Scripts.html).

If no package exists, compiling from source is generally not too
difficult on Linux systems. Detailed instructions are available:
[Building RKWard From Source](Building_RKWard_From_Source.html)

### Windows

#### Is RKWard available for Windows?

Yes, it is\! There are some platform-specific quirks, but RKWard runs on
at least Windows 2000, XP, Vista, and Windows 7. Many users use RKWard
productively on these platforms.

#### How can I install RKWard on Windows?

Several different options exist for installation, ranging from a bundled
installation of everything you need to run RKWard, to compiling from
source. See [RKWard on Windows](RKWard_on_Windows.html) for
details.

#### Can I install RKWard on a USB disk on Windows?

Yes, you can. Be sure to use the "complete installation bundle" (see
[RKWard on Windows](RKWard_on_Windows.html)). This installation
can freely be moved to different paths.

### Mac OSX

#### Is RKWard available for Mac OSX?

Yes, it is\! This is still rather fresh, though, and it's quite possible
that you will run into a few quirks. We provide binaries for Mac OS X
10.7 or above. For older Macs, you will have to use MacPorts to compile
from source. See [RKWard on Mac](RKWard_on_Mac.html) for
instructions on installing on the Mac, and please provide your
feedback\!

## Troubleshooting

### I get an "error while starting the R backend". What does it mean?

#### "The 'rkward' R-library either could not be loaded at all, or not in the correct version"

This error is typically the result of mixing different methods of
installation on a single system (e.g. from binary packages, and from
source, or installation from source with different parameters). More
specifically, R supports installing libraries to several different
locations. Probably, in one of these locations, an older version of the
rkward support library has been left over from a previous installation.

In general, you want to clear all leftovers of previous rkward
installations. To do so, run R as root, and type:

` for (libloc in .libPaths()) try (remove.packages ("rkward", libloc))`

**Note**: On some systems, there are even several installations of R,
e.g. one in /usr/local/bin/R, and one in /usr/bin/R. In this case, do
the above for each installation of R.

You may want to apply the procedure mentioned below, to clean up any
other left-overs of previous rkward installations, too, for good
measure. When done, re-install RKWard.

#### "RKWard either could not find its resource files at all, or only an old version of those files"

Typically, this error occurs, if you installed from source, and did not
use a suitable "-DCMAKE_INSTALL_PREFIX" option while [running
*cmake*](Building_RKWard_From_Source.html). The error may also
indicate that there are left-overs from a previous installation of
RKWard on your system. Try running

` find /usr /opt -name rkward`

on the command line. Clear out all mentioned directories / files, then
install again (using the correct "-DCMAKE_INSTALL_PREFIX" option).

### I have upgraded to R, and now RKWard stopped working

While much of the time RKWard will simply continue to work with a new version of R (esp. minor releases), sometimes it is necessary to re-compile RKWard for the version of R you will be using.

Importantly this can be a problem when trying to use a compiled package of RKWard provided with your Linux distribution, while using an updated package of R, directly from CRAN. For some distributions, we, or third parties,
provide appropriate updated packages. Please refer to the page on [binaries and build scripts](/Binaries_and_Build_Scripts.html) for more info. If no solution is listed for your distribution, you may have to [compile from source](/Buidling_RKWard_From_Source.html), and / or ask your provider to create updated packages.

## Design issues

### Why is RKWard a KDE-application?

KDE offers a set of tools that allow us to make progress relatively
quickly. The QT library on which it is based is one of the very best
toolkits currently available.

### Why is ECMAScript (Javascript) used for the plugin framework?

The reasons for using JS are:

  - well known and stable (applies to python as well)
  - not much functionality is needed in this context, and JS provides
    all that we need in a lightweight manner, with very little overhead
    compared to most other options.

See
[Converting_Plugins_from_PHP_to_JS](Converting_Plugins_from_PHP_to_JS.html)
for some background information.

## Getting involved

### What can I help with?

Please have a look at the [Open Tasks](Open_Tasks.html) for a list
of good starting points. Or offer your help on the mailing list (see
next question). We are looking for all sorts of skills and talents.

#### Donations

We develop RKWard, because we have use for a good statistics tool in our
professional lives, but RKWard is not part of our jobs, and time we
spend on the project is time that we can't spend other activities such
as advancing our careers. We enjoy working on the project for various
reasons, but not all aspects of this work are fun.

Donations we receive will be split among the currently active developers
(although many give the money back to the project). It helps us buy new
hardware to develop RKWard on, and it helps us achieve the feeling that
the time we spend working on RKWard is well invested.

To make a donation, please visit:
<http://sourceforge.net/donate/index.php?group_id=50231> .

Thanks\!

[Help:Wiki_Editing](Help:Wiki_Editing.html).

### How to get in contact?

[Main ways to contact the project](/Contact.html)
