---
layout: page
---

# Current news

{% include 2022_05_30_announce_0.7.4_teaser.md %}
{% include 2022_05_30_announce_0.7.4_body.md %}

<!-- end list -->

* Table of contents
{:toc}

## Archived news 2022

{% include 2022_04_21_announce_0.7.3_teaser.md %}
{% include 2022_04_21_announce_0.7.3_body.md %}

## Archived news 2020

{% include 2020_10_16_announce_0.7.2_teaser.md %}
{% include 2020_10_16_announce_0.7.2_body.md %}

{% include 2020_03_23_announce_0.7.1_teaser.md %}
{% include 2020_03_23_announce_0.7.1_body.md %}

## Archived news 2018

{% include 2018_04_16_announce_0.7.0_teaser.md %}
{% include 2018_04_16_announce_0.7.0_body.md %}

## Archived news 2016

### RKWard 0.6.5 - Much improved preview functionality, and more

*02 April 2016*

Today, we are pleased to announce that again, a new version of RKWard is
available for [download](Download_RKWard.html). This new release
fixes some compilation problems with the upcoming R 3.3.0. However, it
is much more than a "hot fix" release. The most notable area of
improvements have been previews: In addition to the time-tested plot
previews, many RKWard plugins now provide previews of imported data, or
results. Also, we have once again fixed various usability issues in
this, and other areas (details below).

Meanwhile work continues on porting RKWard to version 5 ("frameworks")
of the KDE libraries. The curious will find some first experimental
binaries on our [Release Schedule](Release_Schedule.html) page.

As usual, please don't be shy of [contacting](Contact.html) us
with your feedback, suggestions, and contributions\!

#### The changes in detail:

  - New features and improvements
      - Add option to override CSS file used for the output window
      - Added context menu option to search for information on current
        symbol online
      - Provide better status feedback when searching / filtering among
        installable packages
      - Add access to basic menu items for docked previews
      - Move preview controls to the button-column of dialogs
      - Add preview to Sort data-, Subset data, and Recode categorical
        data-plugins
      - Add preview to Power Analysis plugin
      - Add support for adding "htmlwidget"- and "gvis"-class objects to
        the output window via rk.print()
      - Add plugins for importing Excel files (.xls and .xlsx, Perl- and
        Java-based)
      - Add ability to extract <matrix> values row-wise in plugins
      - Add convenience JS-function "makeOption()" for use in plugins
      - Add previews for CSV, SPSS, and Stata import plugins
      - Allow previews for data, (HTML) output, and custom types of
        previews
      - Allow previews to be docked to the dialog window, and dock them,
        by default
      - Implicitly save code preview visibility and size (instead of the
        former explicit settings)
      - data.frame objects outside of globalenv() can be opened
        read-only in an editor window
      - Show a warning screen when invoking plugins from the command
        line (or from clicking an rkward://-link in an external
        application)
      - Use package type option "binary" from R 3.1.3 onwards, for
        automatic selection of the appropriate binary package

<!-- end list -->

  - Fixes
      - When manually adding pluginmaps, load these after, not before
        other pluginmaps, by default
      - Fixed: Wizard plugins would be too small, initially
      - Fixed: Help Search was not longer working correctly with R 3.2.x
      - Fix compilation with R 3.3.x
      - Fixed: Numerical (display) precision setting was not honored in
        data editor
      - Fix several window activation quirks in "Focus follows mouse"
        mode
      - File selectors in "Import XYZ" plugins now filter for standard
        file extensions, by default

### RKWard 0.6.4 - Usability improvements, fixes, and preparation for porting to KDE frameworks version 5

*21 December 2015*

Shortly before the end of the year, a new version of RKWard is available
for [download](Download_RKWard.html). While the list of changes
looks relatively short this time, beyond the usual fixes, with the help
of usability expert Jan Dittrich, we have addressed some long standing
potential for confusion in several important parts of RKWard's user
interface.

Among the changes that do not quite show in our ChangeLog, we continue
receiving more and more translations from the [KDE language
teams](http://l10n.kde.org). Also we are working to finish our move to
KDE.org. Importantly, we now track new issues on the KDE bugtracking
system (see [Bugs](Bugs.html))). A [lot of
work](https://files.kde.org/rkward/R/pckg/rkwarddev/NEWS.html) went into
the [rkwarddev package](https://github.com/rkward-community/rkwarddev)
to make the plugin generating code much more intuitive. In particular,
see the [documentation on the new *js()* function](/doc/rkwardplugins/rkdev_example.html#rkdev_jscode).

Finally, already looking ahead to forthcoming releases, we are making
[good
progress](https://mail.kde.org/pipermail/rkward-devel/2015-November/004338.html)
on porting RKWard to the more modular, more modern version 5 of the KDE
libraries (aka KF5).

As usual, please don't be shy of [contacting](Contact.html) us
with your feedback, suggestions, and contributions\!

#### The changes in detail:

  - New features and improvements
      - Plugins check for correct object type, in more places, but allow
        to proceed with questionable object selections
      - Switch to bugs.kde.org as primary issue tracker
      - Workspace browser gains functionality to search / filter objects
        by name, making it easy to find objects, quickly
      - Separate globalenv() and other environments in the search path
        more clearly in workspace browser
      - Complete rework of <optionset> UI for better usability
      - Some usability refinements to "Import CSV" plugin
      - Disabled the "Import Data"-dialog, as it was considered too
        confusing. The individual importers are still available from the
        menu, separately.
      - For multi-item varslots and valueslots, use separate buttons for
        adding / removing items
      - Don't show (useless) index number in mutli-value varslots and
        valueslots
      - Make the option to disable / enable editing more visible (in the
        data editor's toolbar)
      - Add context menu to HTML viewer window, including the ability to
        open links in a new window
      - Remove dependency on KHTMLPart

<!-- end list -->

  - Fixes
      - Fixed: Plugin variable slots taking more than one object would
        not be highlighted in red while invalid
      - Fixed: RKWard package repository would be listed twice on fresh
        installations
      - Fix some minor toolbar glitches
      - Try harder to honor system locale settings
      - Remove obsolete "Export tabular data" plugin (superceded by
        "Export Table / CSV")
      - Do not invoke symbol name completion while in the middle (not
        end) of a word

## Archived news 2015

### RKWard 0.6.3 - Much improved support for translations, reworked plugin management, first release on KDE.org

*07 March 2015*

A new version of RKWard is available for
[download](Download_RKWard.html), today. The new version brings
changes all over the place: The distribution calculator plugins and the
CSV-import plugin were reworked, from scratch. A plugin for test power
analysis is now included in the default distribution. Also, the UI for
managing pluginmaps has been reworked, to make it easier to deal with
the growing number of plugins that are available for download,
separately.

Perhaps the most important change is that RKWard, including its plugins,
is now finally fully translatable. Not everything may be translated to
your language, yet, but now it is possible, and the [KDE language
teams](http://l10n.kde.org) are doing a wonderful job at making RKWard
available in your native language, soon.

Before reading on about the improvements and bugfixes in detail, be sure
to check your browser's location bar: RKWard is now hosted at KDE.org.
This is a move towards better services, more expert knowledge in our
reach, and not least towards becoming even more open to your
contributions. Don't be shy of [contacting](Contact.html) us with
your feedback, suggestions, and contributions\!

#### The changes in detail:

  - New features and improvements
      - New element <i18n> for use in plugins' logic section: Provides a
        translatable string property
      - New element <label> for use in plugin help pages: Copies the
        label-attribute of the given element into the text
      - New string property modifier "quoted" to make it easier to quote
        dynamic strings inside plugins
      - Reworked distribution calculator plugins
      - Added power analysis plugin (already existed as separate plugin)
      - Assume plugin .js files to be in utf-8 encoding; this allows
        using non-ascii characters in the generated code
      - <matrix> element gains options min_rows and min_columns, and
        the details of fixed_width="true" have been improved
      - Add R function rk.set.plugin.status() to allow further
        customization of loaded plugins (hiding of individual menu
        entries)
      - Pluginmap-management was reworked, partially, and moved to
        Settings-\>Manage R packages and plugins
      - Provide more detailed information on loaded plugins in
        rk.list.plugins()
      - Allow to override plugins from a different pluginmap (the plugin
        with the highest specified version is used)
      - When the RKWard installation has moved on disk, try to adjust
        stored .pluginmaps paths, accordingly
      - Allow opening RKWard's plugin files (with correct highlighting),
        and other text files
      - More robust control over placement of plugins within a menu
      - Restructure layout of CSV-import dialog
      - Allow to open (any number of) R script files and rkward://-urls
        from the command line
      - Add command line option --reuse for reusing an existing instance
        of RKWard
      - Be slightly(\!) smarter about when (not) to ask for saving
        workspace on workspace load (e.g. not directly after workspace
        has been saved)
      - Change default in Workspace browser to showing only .GlobalEnv,
        initially
      - Support automatically generating a printable header parameter
        from most plugin elements
      - New Object based convience method for printing headers from
        plugins
      - Implement polyPath()-drawing in RK() device
      - Pre-compile the common js code included in every plugin (only
        when compiled with Qt \>= 4.7)
      - Improve crash recovery dialog to not prompt again for the same
        files
      - Plugins and in-application help pages can now be fully
        translated

<!-- end list -->

  - Fixes
      - Fixed: Hang when trying to select existing directories in file
        selectors on Windows
      - Fixed: <valueslot>s were quirky with respect to showing as
        invalid
      - Fix a hang-on-exit issue
      - Fixed: Error when using fix() or trace(...edit=TRUE) with
        default settings on some systems
      - Fixed: Freezes when using RKWard-functionality (such as the
        RK()-device) from tcl/tk (e.g. Rcmdr)
      - Fix several issues of excessive printing of digits in plugins'
        output
      - Fixed potential crash while the "RKWard Debug Messages" window
        is visible
      - Fixed display of file paths containing non-ascii characters in
        the title bar and startup dialog
      - Fixed some erroneous plugin debug messages

## Archived news 2014

### RKWard 0.6.2 - Graphics features come to the Mac, many new features for plugin development

*20 October 2014*

The RKWard team is proud to announce the release of version 0.6.2 of the
free statistical software suite -- the first release since April 2013.
Next to the usual round of bug-fixing, this release is packed with
important new features. One of these is the new RKWard native on-screen
graphics device RK(), which finally brings RKWard's graphics device
features to the Mac. Plugin developers can utilize some new controls,
and fetch information from R, dynamically.

At the some time several of the official plugins have been improved
(e.g., the plot export formats now include support for tikzDevice;
there's a new dialog to recode categorical data, and the linear
regression dialog gains an option to save all predicted values to your
workspace). But also there have been notable additions to RKWard plugins
that can be downloaded from our [external plugin
repository](http://rkward.sourceforge.net/R). For instance, there is a
new package
[rk.power](http://rkward.sourceforge.net/R/pckg/rk.power/index.html)
which adds a GUI for power analysis and sample size estimation to
RKWard.

#### The changes in detail:

  - New features and improvements
      - In data editor, indicate NAs, explicitly
      - Import Stata plugin gains option to convert character encoding.
      - New plugin for recoding categorical data
      - New embeddable (minimal) plugin "multi_input" to combine
        different input elements
      - Added command line option --r-executable for switching between
        several installations of R
      - Use a binary wrapper, instead of wrapper shell script for
        startup on all platforms
      - Linear regression plugin gains option to save predicted values
      - Add basic support to export plots using tikzDevice
      - Allow plugin UI script code to query R for information
      - Allow to connect <varslot>/<valueslot> source to any property,
        not just <varselectors>
      - New plugin elements <valueselector> and <select>
      - New plugin element <valueslot> for selecting arbitrary string
        values (otherwise almost identical to <varslot>)
      - <varslots> can be set to accept the same object several times.
        Used in scatterplot plugin.
      - New R function rk.embed.device() for manually embedding graphics
        devices in RKWard
      - Remove support for compiling RKWard in a single process
        (threaded) variant. This was not used / tested since RKWard
        0.5.5
      - Shortcuts for the "Run ..." actions have been changed for better
        cross-platform compatibility
      - The script editor's "Run line" and "Run selection" actions have
        been merged
      - Add UI for configuring default graphics device, and embedding of
        standard graphics devices.
      - New RKWard native on-screen device (RK()). This is the default
        on-screen device in RKWard, now.
      - New R function rk.without.plot.history() for turning off plot
        history, temporarily
      - Add command line option --backend-debugger

<!-- end list -->

  - Fixes
      - Fixed: Problems starting from paths with spaces in the file name
        on Windows
      - Fixed some compilation problems
      - Fixed: cbind-value of <matrix> element was missing commas
      - Fixed: Give a label to an unlabelled toolbar
      - Fixed: Adjust to (re-?)named parameters for options("pager")
      - Fixed: potential crash when a previously installed pluginmap is
        not longer readable
      - Fixed: R backend would exit immediately, without meaningful
        error message, if there is an error in .Rprofile (or
        Rprofile.site)
      - Fixed: Installing suggested packages from the package
        installation dialog was broken
      - Fixed: Selecting a mirror via the "R packages" settings page
        would not work when prompted for package installation form the
        backend

## Archived news 2013

### RKWard 0.6.1 - Support for R 3.0.0, new features for plugin developers

*02 April 2013*

A new version of RKWard is available
[download](Download_RKWard.html). The most important bit for most
users will be that RKWard 0.6.1 works with the upcoming R 3.0.0, while
the previous version *will not work* with this version of R. Users
installing RKWard from source should note that you will have to compile
RKWard *after* upgrading to R 3.0.0 or later. If in doubt, feel free to
[ask](Contact.html) for help.

Besides compatibility, the new release brings new features and fixes. A
particular development focus has been the addition of new options for
plugin developers. For a more detailed description of the new
features,[see](http://www.mail-archive.com/rkward-devel@lists.sourceforge.net/msg01949.html)
[these](http://www.mail-archive.com/rkward-devel@lists.sourceforge.net/msg01950.html)
[mails](http://www.mail-archive.com/rkward-devel@lists.sourceforge.net/msg01982.html),
or refer to the usual [documentation on writing plugins for
RKWard](http://rkward.sourceforge.net/documents/devel/plugins/index.html).

The changes in detail:

  - New features and improvements
      - Add plugin for subsetting data.frames by rows or columns
      - On the Windows platform, add an new (experimental) binary
        startup wrapper (rkward.exe)
      - Added support for loaded namespaces that are not attached to a
        loaded package
      - Pluginmaps can specify their "priority". Pluginmaps with low
        priority will not be added automatically, when found.
      - Pluginmaps can <require> other pluginmaps based on their id (for
        cross-package inclusion)
      - Added new element <dependency_check> for dynamic version checks
        within a plugin (R and RKWard versions, only, so far)
      - Add guard against accidental usage of the standard distributed
        pluginmaps in a later version of RKWard (installed in parallel)
      - Easier (de-)activation of RKWard plugin maps using checkboxes
        (Settings-\>Configure RKWard-\>Plugins)
      - Broken or quirky .pluginmap files are reported to the user,
        completely broken maps are disabled, automatically
      - Implement basic dependency handling for plugins / pluginmaps
      - Added support for the upcoming R 3.0.0
      - Added
        <switch>
        logic element to switch between several target properties (or
        fixed values) based on the value of a condition property
      - Sort plugin gains option to sort data.frames by more than one
        column at a time, and options for type conversion
      - Add in-application debug message viewer (targetted at (plugin)
        developers)
      - Add setting to customize initial working directory
      - Windows only: Add UI-checkbox for R's "internet2"-option
      - New functions getString(), getList() and getBoolean() for
        fetching data in plugin scripts
      - Boolean properties now return a numeric, not labelled
        representation of their value, by default. <checkbox>es should
        be unaffected.
      - Added <optionset> GUI element for entering a set of options for
        an arbitrary number of items
      - Reduce CPU usage of plugins while idle
      - In the data.frame editor, columns containing invalid values are
        now highlighted in red
      - Added <matrix> GUI element for entering matrix or vector data in
        plugins
      - Improve key handling while editing factor levels in a data.frame
      - Added utiltity function rk.flush.output()
      - rk.set.output.html.file() gains argument to allow addition of
        custom content to the html header
  - Fixes
      - Add option to force-close a graphics window
      - Revert to building R packages form source on Mac OS X by default
        (controllable via compile-time option)
      - Fixed: lattice plots would not be added to the plot history,
        correctly, for some versions of lattice
      - Fix crash when trying to print, and neither okular, nor kpdf are
        available
      - Fix conversion from Numeric to Factor in the data editor
      - Fixed: If none of the previous plugin maps could be found on
        startup, re-add the default
      - RKWard is now categorized as Science/Math/Numerical Analysis in
        its .desktop file
      - Fixed: Yet another fix for hard-to-read function argument hints
      - Fixed: Device history was not working with more recent versions
        of ggplot2
      - Fixed: Option to include suggested packages in install was
        mis-labelled "install dependencies"

## Archived news 2012

### RKWard 0.6.0 is available - also on the Mac

*24 October 2012*

Today, a new version of RKWard is available for
[download](Download_RKWard.html). RKWard 0.6.0 adds initial
support for debugging R code (a debugger console, and a display of
current evaluation frames). Dialogs for - among others - t-tests and
Wilcoxon-tests have been improved, a table of contents was added to the
output window, and, as usual, many more features were added, and bugs
were fixed, both in RKWard itself, and external plugins. Perhaps more
importantly than all this, however, 0.6.0 is the first official release
of RKWard to be readily [available on Mac OS
X](RKWard_on_Mac.html).

Also, at this occasion, I'd like to take a short moment to look back:
RKWard is approaching it's tenth anniversary, version 0.1.0 was released
on November 19, 2002. During those ten years, at times, the project has
been on ice, or going at glacial speed. But the project is still here
and - driven by a wonderful community - it's looking younger than ever,
after all this time. Many thanks to everybody involved\!

As usual, please don't hesitate to send us your
[feedback](Contact.html).

The changes in detail:

  - New features and improvements
      - Preview device windows will display some status information
        (most importantly, warnings or errors)
      - Most plot plugins gain options to control margins and tick label
        orientation
      - Added option for installing packages from source (implicitly
        enabled on Unixoid platforms)
      - Omit comments on missing function calls in dialog code windows
        (e.g., if prepare() is unused, there's no "\#\# Prepare" in the
        output either)
      - Output markup is now more XHTML compliant and easier to parse
      - Also save cursor position, folding, etc. for scripts. Note:
        Implementation details may be subject to change.
      - New function rk.list.labels() to retrieve column labels in a
        data.frame
      - rk.get.label() will now return an empty string ("") instead of
        NULL, in case no label is assigned
      - Do not offer to restore individual removed columns of a
        data.frame opened for editing
      - combined all Wilcoxon/Mann-Whitney-tests in one plugin (from
        previously two separate plugins)
      - Added polyserial/polychoric correlations to correlation matrix
        plugin
      - Added more compression options to the "Save objects as R code"
        plugin
      - Added MacPorts support, see README.MacPorts and bundle build
        script in the macports folder
      - Added dynamically generated table-of-contents menu to output
        document
      - Allow some markup inside <text> elements in plugins, and
        auto-add breaks only for duplicate newlines.
      - Reorganized t-test plugin, and add support for single sample
        t-tests
      - Box plot plugin gains more options for adding means
      - Improve keypress handling issues in the R Console, when the
        cursor or a selection is outside the editable range
      - Only install translations which are at least 80% complete (not
        counting strings in plugins, which are not yet translatable)
      - When asking for workspace file to open, use appropriate file
        filter
      - When configured to ask for workspace to open on startup, don't
        prompt to save, first
      - Simplified the "Sort Data" plugin, and added a help page
      - Added GUI support for inspecting the call stack during debugging
      - The backend executable is no longer linked against KDE libraries
      - Objects, which are not acceptable in a varslot, will still be
        shown, there, with a warning
  - Bugfixes
      - Fixed: Entering "0" as propabilities (quantiles) vector in
        distribution plugins would cause error message
      - Fixed: Wrong handling of carriage returns ('\\r') in the console
        window
      - Fixed: Spinboxes had wrong initial values
      - Fixed: Changed configuration settings would not be saved for
        script editor
      - Fixed: One character missing in R commands on lines longer than
        4096 characters
      - Fixed: "Next"-button in wizards would remain enabled while
        settings are missing on a page
      - Fixed: Dynamic pages in a wizard would cause a layout bug on the
        first page
      - Fixed: Plot history and graphical menus broken in some cases
        with R 2.15.0
      - Fixed: If the rkward package was loaded in a plain R session,
        q() and quit() still work
      - Fixed: Would not show output of system() commands to stderr on
        the console (on Unix-like systems)
      - Fixed: Function argument hints for the second half of the
        parameter list would not be quoted, correctly
      - Fixed: Failure to open workspaces with non-latin1 filenames from
        the command line
      - Fixed: Saving / restoring workplace layout would fail when
        saving to directories with unusual characters
      - Fixed: potential crash when clicking "Select all updates" in the
        package installation dialog
      - Fixed: potential crash in object name completion under certain
        conditions
      - Fixed: On Windows, detached windows would sometimes be
        positioned with the menubar outside the upper screen edge

### RKWard 0.5.7 - Support for R 2.14.0, improved package management, and more

*23 October 2011*

RKWard 0.5.7 is available for ([download](Download_RKWard.html))
today. Importantly, this version supports the upcoming version 2.14.0 of
R. But there are many more reasons to update, today: We have re-thought
our approach to downloading RKWard add-ons, and have replaced out
previous solution with one that is based on regular R packages. Some
first add-ons based on the new solution are already available, including
the *rkwarddev*-package, aimed at making it even easier to [create
plugins](http://rkward.sourceforge.net/documents/devel/plugins/index.html)
for RKWard. As a side product, the package installation dialog has been
modernized. But also there are many new features and bugfixes all over
the place.

Read on, below, for more detail. And remember: We need your
[feedback](Contact.html)\!

The changes in detail:

  - New features and improvements
      - Offer to add a new library location, automatically, if location
        selected for installation is not writable
      - Reworked package installation / update dialog
      - Documentation on writing RKWard plugins is now accessible,
        locally
      - The file filter for R script files is now configurable, and
        includes \*.Rhistory, by default
      - More obvious coloring of variable entry fields requiring user
        input in plugins
      - Default size of the code display in plugin dialogs has been
        increased
      - Removed support for downloading plugins using GHNS
      - Added support for RKWard plugins shipped inside R packages
      - Removed option to set options("printcmd")
      - New option to run arbitrary (setup) commands in each session
      - Added new pseudo graphics device "rk.printer.device" to provide
        printing via the KDE printer dialog
      - Support the results list new help.search() in R 2.14.x, which
        includes vignettes and demos
      - rk.edit.files() and rk.show.files() gain parameter "prompt",
        which can be used to suppress user prompt
      - Added function rk.demo(), which is similar to demo(), but opens
        the example script in a script editor window
      - Added shortcut Ctrl+Enter to insert a line break and submit from
        a script editor
      - Reduce CPU usage while idle
      - Bar plot, dot chart, pie chart, and Pareto plot plugins gain
        more tabulation options
      - rk.results() now prints rownames, where appropriate
      - "frame" elements in plugins can now be made checkable
      - Disabling or hiding a plugin component makes it non-required,
        implicitly
      - Box plot gains option to dogde grouped boxes
      - Simplify internal handling of system() and system2() output
      - Simplify code produced by several plugins

<!-- end list -->

  - Bugfixes
      - Fixed: Would not system-defined text color for argument hints
      - Do not treat arrays (which are not a matrix) as hierarchical
        named objects in the object browser
      - Do not analyse more than 100000 name child-objects per object
        (avoids hangs on such extreme data)
      - Fixed: Problems with using mclapply() inside RKWard
      - Fixed: Integrated help browser would not update navigation
        history when following page internal links
      - Fixed: PDFs and many other types of documents linked from help
        pages would not be opened, correctly
      - Added support for R 2.14.x
      - Fixed: Printing was broken when kprinter is not available
      - Fixed: Some plugin dialogs would not become visible, or be shown
        in very small size, with some window managers
      - Fixed: Potential crash when using context menus with "focus
        follows mouse" window activation policy
      - Fixed: Occasional duplication of first letter of keyword, when
        using "Copy lines to output" from the console window

## Archived news 2011

### RKWard 0.5.6 - Assorted new features and fixes

*30 May 2011*

Sometimes, it is not easy to summarize the changes that happened in only
a bit over two month of development. Besides the usual bug-fixes, RKWard
0.5.6 ([download](Download_RKWard.html)) brings many small
improvements all over the place. For users working with many different
windows, the new Ctrl+Tab window switching feature will be of particular
interest. R experts will appreciate that S4 slots and package namespaces
can now be explored in the workspace browser. The output window can now
also be used, easily, to document snippets of R code (including syntax
highlighting) and the corresponding R output.

For more detail, read on, below, and most importantly, be sure to give
the new version of RKWard a try. Remember: Your
[feedback](Contact.html) is important\!

The changes in detail:

  - New features and improvements
      - Add simple man page for command line options
      - Replace the R console's "Copy selection cleaned" action with
        "Copy commands, only", which will omit any non-command regions
        in the copy
      - Add "Copy lines to output" action to copy highlighted lines from
        the R console or script editors to the output window
      - R commands and their output can be "carbon copied" to the output
        window
      - On Windows, RKWard will detect, and offer to disable "native"
        file system dialogs
      - Object browsers that show only the global environment now hide
        the ".GlobalEnv" item
      - Added function rk.print.code() to write highlighted R code to
        the output window
      - Box plot plugin gains support for grouped outcome data
      - Crosstabs N to 1 plugin gains options to compute proportions and
        margins (thanks to Andrés Necochea)
      - Added convenience R function rk.list() to allow simplification
        of plugin code
      - Added stack-based window switching using Ctrl+(Shift)+Tab; this
        replaces the old "Next Window" and "Previous Window" actions
      - Provide a template for bug reports, containing standardized
        information on the RKWard setup in Help-\>Report Bug...
      - When loading a (local) workspace, change the working directory
        to the directory of the workspace (configurable)
      - Include the sidebar position of tool windows when saving /
        restoring the workplace layout
      - "Pending jobs" tool window is not longer shown in the bottom
        sidebar by default
      - Support removing individual tool windows from the sidebars,
        completely
      - Add an action to interrupt all (current and pending) commands
      - File browser context menu gains many more KDE standard actions
        (KDE 4.3 and above, only)
      - Auxiliary binaries rkward.bin and rkward.rbackend are no longer
        installed into /usr\[/local\]/bin/ on Unix
      - Support function argument hinting for R primitives
      - Support package namespaces in object name completion and
        workspace browser
      - Support S4 slots in object name completion and workspace browser
      - Support plot history for ggplot2 plots
      - Be less pro-active about fetching structure information on R
        objects in the workspace
      - Allow to browse arbitrarily deeply nested environments in the
        object browser

<!-- end list -->

  - Bugfixes
      - Fixed: Would crash when searching for a term with quotes in the
        help search window
      - Fixed: Potential crash when creating plots from a tcl/tk based
        GUI
      - Fix compilation on FreeBSD
      - Fixed: Function argument hints would not be shown in some corner
        cases
      - Fixed: Pressing Ctrl+C would not reset syntactically incomplete
        commands in the R console
      - Fixed: Graphics device windows would disappear when trying to
        attach them to the main window with some versions of Qt
      - Fixed: tcl/tk widgets would lock up after running commands in
        the R Console
      - Fixed: When saving the workplace while closing the session, the
        workplace layout would not always be saved correctly
      - Fixed: Object name completion would abort early in the script
        editor with KDE 4.4
      - More correct handling of quotes in object name completion

### RKWard 0.5.5 - Enhanced robustness and many small improvements

*19 March 2011*

On the surface, RKWard 0.5.5, released today
([download](Download_RKWard.html)) may appear to bring only few
changes. Internally, however, the GUI and the R engine have been split
into two separate processes. This brings better robustness, and better
compatibility of RKWard esp. with R packages based on rJava and RGtk2.
In addition, many bugs and quirks have been fixed in various areas. And,
in case you experience a crash despite of these improvements, RKWard
0.5.5 will attempt to create an emergency save file of your data in the
last moment.

Please give this new version of RKWard a try, and don't forget: We are
always eager to hear your [feedback](Contact.html)\!

The changes in detail:

  - New features and improvements

\- Re-organize the default toolbar actions of the main window - Detached
windows are now restored in detached state when restoring the workplace
layout - Workplace layout is now saved in a separate file, instead of in
a hidden object inside the .RData file - Added R functions
rk.save.workplace() and rk.restore.workplace() to save / restore a set
of document windows - RKWard now tries to detect, when a workspace has
been moved to a new directory, and adjust the paths restored script
editor windows, accordingly - All pages in the package installation
dialog now support sorting and keyboard search - Allow entering factor
labels instead of only numbers, when editing factors in the data editor
- Display logical values as "TRUE" and "FALSE" in data editor, and
accept "T"/"TRUE"/"F"/"FALSE" in addition to "0"/"1" while editing - Add
support for R functions loadhistory(), savehistory(), history(), and
timestamp () - Moved automated testing framework moved to a dedicated R
package "rkwardtests", and added documentation - Support pasting to the
middle of the current command line in the R Console - Better handling of
script-editor commands while the R Console is busy - GUI frontend and R
backend now run in separate processes - Re-write of large portions of
the R embedding code - Attempt to save workspace on crashes - Removed
the remainder of the PHP scripting backend

  - Bugfixes

\- Fixed: Crash when several environments on the search path have the
same name - Fixed: Potential crashes when changing length of a
data.frame that is currently opened for editing from R code - Fixed:
Converting from factor to string in the data editor set values to
numeric, internally - Fixed: On Windows, a wrong locale for LC_NUMERIC
would be applied, resulting in malfunction of pdf() and postscript() -
Fixed: Did not use system configured background color in data editor -
Fixed: New columns would be always be added to the right, instead of to
the left, in the data editor - Fixed: Would crash when trying to edit 0
row data.frame - Fixed: Output generated by external processes
(system()) was not shown on the console - Fixed: Converting a variable
to factor in the editor would drop existing levels, silently, and lead
to NAs - Fixed: Cursor would keep jumping to the end, when typing a
filename in the filename selection field in plugins - Fixed: Submit
button would not become enabled in "Basic Statistics" plugin - Fixed:
Portions of multi-line commands entered on the R Console would be run
twice under some circumstances - Fixed: Lockups or crashes when using
packages RGtk2 or rJava on the X11 platform - Fixed: R task callback
handlers were not called - Do not load .RData file from current
directory by default (configurable) - Correct interleaving of multi-line
commands and the corresponding output in the R Console - Fixed: Did not
respect system settings for tooltip background color in some styles -
Fixed: Installation would continue, seemingly successfully, even if R
support libraries could not be installed - Fixed: Function argument
hints would sometimes persist after closing a script window - Fixed:
Would fail to analyse structure of ReferenceClass-objects - Fixed:
"Vector" mode in "Paste special" action did not work correctly - Also
try to relay SIGABRT and SIGILL to the proper signal handlers - Fixed:
Would crash on tcl/tk events in some cases

## Archived news 2010

### RKWard 0.5.4 - Plot history, bug fixes, and new possibilities for plugin writers

*04 October 2010 - [User:Tfry](User:Tfry.html)*

The most visible change in RKWard 0.5.4
([download](Download_RKWard.html)) may be the addition of a plot
history for the on-screen graphics device. Supporting both regular and
trellis plots, this can facilitate the creation and management of plots,
considerably. Further, the data.frame editor now supports row names, and
plugin authors now have new possibilities for scripting plugins, which
we hope to put to good use in the coming releases. Documentation has
been added for the functions in the rkward R package. Beyond that, there
has been the usual bug fixing, and a number of assorted small new
feature additions.

The changes in detail:

  - New features and improvements

\- Added full-featured plot history for the onscreen graphics device -
Added option to specify default onscreen graphics dimensions - Added
option to disable function argument hinting - New functions
rk.show.message() and rk.show.question() for user interaction from R
code - New options for scripting GUI logic in plugins - The current
object of an active data editor can be referenced in plugins - Allow
sorting of results in help search window - The save-object selector in
plugins now allows to save as part of a data.frame / list - Support row
names in the data.frame-editor - Added option to autosave script files
(enabled by default) - The tabbar in the main window now shows a context
menu with options to close/detach a window - The tabs in the main window
can now be re-ordered by dragging with the mouse (left click if compiled
with Qt 4.5 or above, middle click for earlier versions) - Added
alternating row backgrounds in data.frame-editor

  - Fixes and corrections

\- Fixed: Running user commands would crash with R 2.12.0 - Fixed:
Potential crash when editing data-frame with duplicate column names -
Fixed: Graphics windows used to assume a minimum size of 640\*480
pixels, even if a smaller size was specified - Fixed: Placement of
several menu items was broken - again - with KDE 4.4 and above - Fixed:
CPU usage would go to 100% for no good reason under certain
circumstances - Fixed: On some systems, dev.off() would spawn two new
graphics windows - Fixed: When starting with an empty table, RKWard
would sometimes claim that this object has been removed - Fixed: Would
crash when trying to configure toolbars under certain circumstances
(workaround for bug in kdelibs) - Fixed: Crash while analysing some
objects returned by XML::xmlParseTree() for invalid XML - Fixed: Error
while installing packages with R 2.11.0, when archiving packages in a
non-existing directory

We always welcome your [feedback](Contact.html)\!

### RKWard 0.5.3 - New features, and no longer depends on PHP

*30 April 2010 - [User:Tfry](User:Tfry.html)*

RKWard 0.5.3, is available for [download](Download_RKWard.html),
today. Internally, the largest change in this release is that plugins no
longer use PHP for scripting but Javascript. This reduces the
requirements for running RKWard, considerably, and also fixes a number
of issues. While this change should be largely invisible to users, this
version incorporates a number of new features and bugfixes, esp. on
[windows](RKWard_on_Windows.html).

The changes in detail:

  - New features and improvements

\- File browser window saves settings on exit - Plugin dialogs are shown
with a larger initial size - Add close buttons to each tab in the main
document view area (not available in KDE 4.0) - Make "print", "export as
HTML", "dynamic word wrap", and "increase/descress font size" available
for the console window - Use a native menu, instead of the default
TclTk-menu - Added "paste special" action to script editor and console
for pasting R vectors and matrices from spreadsheets - File-\>Open R
Script File now allows to specify the character encoding to use -
Initialize the output file with an appropriate encoding specification -
Add SVG support to export (graphics) plugin - Add basic settings format
settings for graphics output - Convert all plugins to use ECMAscript
instead of PHP; RKWard no longer depends on PHP

  - Fixes and corrections

\- Fixed: Windows-\>Activate-\>Window left / right actions were always
disabled - Several windows-specific bugs were fixed - Fixed: Newly
created variables were not properly updated when closing and re-opening
the editor - Fix order of menus for detached windows - Fixed: No entries
were added the recent script/workspaces actions after "save as" - Fixed:
Frequent crashes while running automated plugintests - Fixed: Filenames
without extension would not be shown in file dialogs - Fixed: Calling
"fix(my.fun)" would remove comments - Fixed: Removing "@CRAN@" from the
repositories would break package installation

We always welcome your [feedback](Contact.html)\!

### RKWard 0.4.9c - Backport of R 2.10.0-support

*18 March 2010 - [User:Tfry](User:Tfry.html)*

RKWard 0.4.9c, is available for [download](Download_RKWard.html),
now. This is a backport of some bugfixes from the 0.5.x-series of
RKWard, most importantly providing support for the new help system in R
2.10.x. Also, a number of plugin enhancements has been backported from
the main branch.

**Most users will not want to use this version\!** RKWard 0.4.9c is
intended for systems which are still limited to KDE 3, only. Where KDE 4
libraries are available, we recommend using RKWard 0.5.2.

The changes in detail:

  - Backport all plugin changes and additions up to RKWard 0.5.2
  - Add support for the dynamic help server introduced in R 2.10.0
  - Backport some improvements in event handling
  - Fix deadlock while handling some Tcl events
  - Fix: Warn before saving file that was modified on disk, externally
  - Fix: Do not try to open mimetypes other than text/html directly in
    the help browser.
  - Fixed: Screen device in rkward was not seen as interactive by R

We always welcome your [feedback](Contact.html)\!

## Archived news 2009

### RKWard 0.5.2 - Support for R 2.10.0 and many small improvements

*26 October 2009 - [User:Tfry](User:Tfry.html)*

RKWard 0.5.2, is available for [download](Download_RKWard.html),
now. This version brings a number of small changes all over the place.
Perhaps most importantly, support was added for the new dynamic help
system in R 2.10.0 (also released, today). Apart from that, the toolbars
are now configurable, plugin dialogs do not stick around, a plugin was
added for Stata import (by Michael Ash), a number of bugs was fixed, and
many more small things as detailed, below:

The changes in detail:

  - New features / improvements

\- Add Stata data file import plugin (by Michael Ash) - Plugin dialogs
close automatically after submitting (by default) - Fetching object
structure is much faster for very large data.frames - "Analysis" menu
was restructured, slightly - On plugin help pages, display a link to
invoke the plugin - Double-clicking an item in the workspace browser now
opens an object viewer, or (if possible) editor - Safeguard against
removal of essential packages via the GUI - Add context menu action to
unload packages in the workspace browser window - Add shortcut to the
load / unload packages dialog also in the workspace menu and the
workspace browser context menu - Make toolbar buttons configurable

  - Fixes and corrections

\- Add support for the dynamic help server introduced in R 2.10.0 -
Assorted minor fixes and improvements to several plugins - Fix deadlock
while handling some Tcl events - Fix crash when loading certain packages
on Windows - Fix some potential path issues on Windows - Fixed: Console
window would sometimes remain in partially active state after piping
commands

  - Internal changes

\- Tolerate missing libraries in testing framework - Debug output
(previously sent to stderr) is now written to a temporary file - All
directly accessible plugins now have at least one automated test

Please send us your [feedback](Contact.html)\!

### RKWard 0.5.1 - New Features and Initial Windows Port

*04 August 2009 - [User:Tfry](User:Tfry.html)*

RKWard 0.5.1, is available for [download](Download_RKWard.html),
now. Perhaps most excitingly, this is the first official version of
RKWard to also be available for the Windows operating system. The
[Windows port](RKWard_on_Windows.html) still has some issues, but
we hope to iron those out in the coming release(s). On Linux/BSD
systems, RKWard 0.5.1 also comes with new features. Perhaps most
importantly, the output window now has links to re-run a plugin with the
same (or adjusted) settings, and the keyboard shortcuts can finally be
customized to your own liking.

The changes in detail:

  - New features
      - New command line option "--evaluate <Rcode>" mostly for the
        purpose of automated testing
      - Add basic checks for a correct installation of the RKWard
        resource files
      - Remove "What to expect" dialog at startup
      - Make keyboard shorcuts configurable
      - Add option to add only single line commands from script editor
        to the console history
      - Add action to change working directory to that of the current
        script file
      - More plugins now write a header to the output window
      - Add convenience function "makeHeaderCode()" for use inside
        plugins
      - Plugins can be invoked from R code (mostly for internal
        purposes)
      - Add "Run again" link for plugin generated output
  - Fixes and cleanups
      - Remove support for R 2.6.x and earlier
      - Fixed: No warning was shown, when an open script file was
        changed on disk, externally
      - Fixed: Opening most file types (e.g. PDF) from the help browser
        was broken
      - Fixed: Make built-in editor work again for file.edit ()
      - Fixed: All objects in .Globalenv would be revisited if a single
        object was added / removed
      - Fixed: Screen device in rkward was not seen as interactive by R
      - Adjust some icons

Please don't hesitate to send us your [feedback](Contact.html).

### RKWard 0.4.9b and 0.5.0d - Bugfixing and backports

*11 May 2009 - [User:Tfry](User:Tfry.html)*

A new set of RKWard releases is available for
[download](Download_RKWard.html). This brings another series of
bugfixes to the KDE 4 branch of RKWard (0.5.0d) and long awaited
backports to the KDE 3 branch (0.4.9b).

The changes in detail:

#### RKWard 0.5.0d (KDE 4)

  - Fixed: Container of detached windows would sometimes remain after
    reattaching
  - Fixed: Global toolbar style was not honored
  - Fixed: Auto-scroll missing in progress dialogs and data editor
  - Fix jumping toolbar
  - Fixed: Several harmless warnings were silenced
  - Fixed: Argument hints would always stay on top regardless of focus
  - Fix crash when reaching bottom of command history in context
    sensitive search
  - Fix crash while inserting rows in a data.frame with string variables

#### RKWard 0.4.9b (KDE 3)

  - Workaround: Deactivate all filemanagement shortcuts in the File
    Browser. These would sometimes trigger when they should not.
  - Fix crash while inserting rows in a data.frame with string variables
  - Fix immediate crash issue with R 2.9.0 (requires recompilation
    against R 2.9)
  - Fix device window capturing with R 2.8.0
  - Extended boxplot plugin
  - Support data.frames in barplot plugin
  - New set of plugins: Item Response Theory (by Meik Michalke)

If you have feedback, if you need support, or if you want to help, [let
us know](Contact.html).

### RKWard 0.5.0c - Maintenance and more

*March 30 2009 - [User:Tfry](User:Tfry.html)*

After a long time without news, a new RKWard release is finally
available for download. Most importantly RKWard 0.5.0c addresses
problems that have arisen due to changes in KDE and in R.

However, RKWard 0.5.0c also comes with a whole set of plugins dealing
with Item Response Theory, contributed by Meik Michalke.

#### Bug fixes and maintenance:

  - Fix assorted installation problems
  - Updated translations: German, Spanish
  - Fix immediate crash issue with R 2.9.0 (requires recompilation
    against R 2.9)
  - Fixed some shortcut problems with KDElibs 4.2.x
  - Fixed: Several icons would not be loaded
  - Fix device window capturing Qt 4.4.3 (possibly other versions were
    broken as well)
  - Fix device window capturing with R 2.8.0

#### New features and improvements:

  - Extended boxplot plugin
  - Support data.frames in barplot plugin
  - New set of plugins: Item Response Theory (by Meik Michalke)

Version 0.5.0c is available on the [download
page](Download_RKWard.html). For feedback and support, contact us
on the [mailing list](Contact.html).

Note: A backport of the applicable fixes to KDE 3 is planned, but will
take some more time to complete.

## Archived news 2008

### Maintenance releases 0.4.9a and 0.5.0b

*April 20 2008 - [User:Tfry](User:Tfry.html)*

Both the KDE 3 and KDE 4 versions of RKWard have been updated today.
Most importantly RKWard now compiles with R 2.7.0 (which is due to be
release, officially, soon). Both releases are available from the
download page.

For the KDE 3 version (RKWard 0.4.9a), the only changes are:

  - Support from compilation with R 2.7
  - New plugin: Hodrick-Prescott Filter
  - Fixed some lockups during startup

The KDE 4 version (RKWard 0.5.0b) has the following changes:

  - Support compilation with R 2.7
  - New plugin: Hodrick-Prescott Filter
  - Fix a problem with plugin help pages not loading correctly
  - Better resize handling in progress and R input (readline()) dialogs
  - Somewhat better handling of the R event loop (esp. when calling
    "require(not_yet_installed_package)")

As usual, your feedback is welcome on our [mailing
list](Contact.html).

### Bugfix release 0.5.0a (for KDE4)

*January 23 2008 - [User:Tfry](User:Tfry.html)*

A simple but important bug in RKWard 0.5.0 was discovered a bit too late
(in most plugins no objects were selectable at all). This is corrected
in version 0.5.0a, which is available for download, now. Please use this
version instead of RKWard 0.5.0. Other than the correction there are
virtually no changes since 0.5.0.

### RKWard 0.5.0 for KDE4 is released

*January 21 2008 - [User:Tfry](User:Tfry.html)*

The first version of RKWard for KDE4 is available for
[download](Download_RKWard.html), now. The list of actual
enhancements (see below) is relatively small, but internally a lot of
code has been rewritten. The move to KDE4 / Qt4 provides a new
foundation for RKWard, and offers a number of promising opportunities
for further development, which we hope to put to good use, soon. The
list of current changes over 0.4.9 (as far we kept track of them):

#### Bugfixes and internals:

  - make sure that R only handles SIGSEV, when the R thread is active.
    Should give better backtraces and less hangs
  - port to KDE 4

#### UI enhancements and new features:

  - object viewer was redesigned to use space better, and only fetch the
    print-representation when needed
  - add support for a focus-follows-mouse activation behavior of the MDI
    windows (Settings-\>General)
  - ability to mark arbitrary "blocks" in script editor and run them
    with a shortcut
  - more icons added
  - data.frame editor can be set to read-only mode
  - code completion in script editor shows icons for the different
    object types
  - settings dialog is now shown in "page tree" mode

Please note that this release has seen considerable code changes. This
should fix a lot of small bugs not listed above, but also, of course, we
expect there are a number of new bugs that we did not discover during
testing. If you find some, or would like to share your opinions on
RKWard, please [contact](Contact.html) us on the mailing list.

**NOTES:** Versions of RKWard 0.5.0 or higher are designed for KDE 4,
and cannot be used with KDE 3. For KDE 3 use the 0.4.x versions. The
build system was changed to CMake. Read INSTALL (also provided in the
source .tar.gz) for details on how to build RKWard. Due to clashes with
kate default shortcuts, the shortcuts for Run-\>Run Line, Run Selection,
and Run All were changed to Shift+F7, Shift+F8, Shift+F9, respectively.

The tabs in the main workplace view no longer have a close button that
is shown when hovering the mouse over the respective icon. Rather, there
is a single button to close the current tab, shown at the right of the
tab bar

### RKWard 0.4.9 is released

*January 14 2008 - [User:Tfry](User:Tfry.html)*

Version 0.4.9 of RKWard is now available for
[download](Download_RKWard.html). The following changes are
contained in the new release:

  - Enhancements:
      - Improved user interface for CSV import plugin
      - Support drawing grids in histogram and stripchart plots
      - New plugin: Levene test
      - New plugin: Jarque-Bera test (by Germán Márquez Mejía)
      - Use the KDE default fixed spacing font for command log and
        object viewer

<!-- end list -->

  - Bugfixes:
      - Fixed: Crosstabs plugin produces better output
      - Fixed: use.value.labels option was wrong in SPSS import plugin
      - Fixed: SPSS tables would only be imported to the workspace, when
        the edit option was checked
      - Fixed: correct installation of rkward.desktop file

In case you are wondering why the list of changes is shorter than usual:
This release is one of the last releases of RKWard for KDE 3. Meanwhile
we've been working hard to produce a version which will run natively in
the newly released KDE 4. This will be released as RKWard 0.5.0 in -
according to plan - roughly one week. We will continue to support KDE 3
versions of RKWard for some time to come, and probably also provide some
new or improved plugins, but the main development will focus on KDE 4
versions of RKWard from now on.

Remember: Your feedback is always welcome on the [mailing
list](Contact.html).

## Archived news 2007

### Bugfix release 0.4.8a

*November 11 2007 - [User:Tfry](User:Tfry.html)*

While development continues on the next versions of RKWard (including a
port to the upcoming KDE 4), we would like to address some bugs that
were discovered since the release of RKWard 0.4.8. This release
([download](Download_RKWard.html)) does not add new user features,
but fixes several problems. The changes in detail:

  - Bugfixes:
      - More reliable C stack limit detection on some systems
      - Fixed: Console accepted pasted input while a command is running
      - Fixed: Analysis-\>Moments-\>Moment plugin did not use the
        order-parameter
      - Fixed: Crash on simple assignments in globalenv() with R \<
        2.4.0
      - Fixed: Meta information (labels) did not get shown correctly in
        the data editor
      - Fixed: Pressing the add button in a varslot with no object
        selected could crash RKWard
  - Internal changes:
      - Add command line option "--debugger" for debugging purposes

As usual, we welcome your feedback on the [mailing
list](Contact.html).

### Version 0.4.8 is released

*October 03 2007 - [User:Tfry](User:Tfry.html)*

Version 0.4.8 of RKWard is available for
[download](Download_RKWard.html), now. The new release features
changes all over the place, including new plugins, an integrated
filesystem browser, and several bug fixes. Also optimizations were done
to speed up some aspects, slightly, and to reduce typical memory usage,
considerably. Many thanks to all who helped with the release. The full
list of changes:

  - User visible additions / improvements:
      - New plugins: pie chart, dot chart, crosstabulation, two
        time-series tests, and basic linear regression
      - barplot plugin allow specification of custom labels
      - graphics export plugin gains option for EPS compatibility
      - CLT plugins allow drawing grid
      - commands run through the console from the script editor will now
        be added to the command history (this can be turned off in the
        settings)
      - object viewer also shows summary information, gains an update
        button, and MDI integration
      - add max.print option to R-Backend settings dialog \* all file
        selection line edits gain filename-completion
      - add a basic file selector window
      - show the focus indication (thin red border) also for detached
        windows
      - tool windows can be closed and detached in the same way as
        regular windows
      - Messages, warnings, and errors for plugin commands are shown in
        the output, instead of in a dialog
      - new translation: Simplified Chinese (contributed by Roy Qu)
  - Bug fixes:
      - fixed: promise objects (created by delayedAssign()) directly in
        globalenv() would be forced early
      - make wrapper script work, when konsole is not installed
      - correct syntax highlighting for ::: and NA_integer_ and others
      - fixed: crash when editing a data.frame with logicals (logicals
        are still mishandled, but less severe)
      - fixed: would not work with R 2.6 \* more compilation fixes for
        GCC 4.3
      - fixed: inserting / removing rows in the data editor in the
        presence of invalid fields in later rows would lead to a crash
      - fix some internal problems with enabledness/disabledness of
        plugin components
      - fixed: sometimes detecting created X11() windows would fail,
        esp. under high load
      - fix a crash with detached tool windows
  - Optimizations:
      - syntax highlighting is minimally faster for long outputs
      - some complex plotting plugins now start up noticeably faster
      - create the workspace browser only when it is first shown
      - add possibility to blacklist packages from fetching structure
        information. Package GO is blacklisted by default.
      - when fetching the structure of "promise" R objects, don't keep
        them in memory
      - optimize the function to retrieve the structure of R objects
        (for display in the object browser, and completion)
  - Miscellanious / internal:
      - silence some GCC 4.2 warnings
      - support for including files and snippets in xml files
      - add command line option --disable-stack-check for systems where
        R C stack checking is buggy
      - the modifier "not" for boolean properties returns a (negated)
        boolean sub-propery instead of just a string

**NOTES:**

  - R 2.6.0: Previous releases of RKWard will not work with the upcoming
    R 2.6.0. You need RKWard 0.4.8 (i.e. this release) in order to work
    with R 2.6.0. Note that when upgrading R after RKWard, you will need
    to recompile.
  - With the addition of the new filesystem browser, the shortcuts to
    show/hide the main tool windows have changed. The filesystem browser
    window can be toggled using Alt+2, the shortcuts for command log,
    pending jobs, console, and help search are now Alt+3 through 6,
    respectively, instead of Alt+2 through 5.
  - All commands run through the console are now added to the command
    history of the console, by default. This setting can be changed back
    to the old behavior (only commands \*entered\* in the console are
    added to the history) under Settings-\>Configure RKWard-\>Console.

Enthusiastic? Disappointed? Need help with RKWard? Your feedback is
welcome on the [mailing list](Contact.html).

### Bugfix release 0.4.7a

*May 07 2007 - [User:Tfry](User:Tfry.html)*

Recently, some problems have been discovered related to resolving some
fortran symbols, on at least some systems. Since these can make RKWard
crash even on simple operations, and not compile on some systems, a
bugfix release dealing with these issues has been prepared. While at it,
a number of additional bugfixes contained in the development version
have been included, here, as well.

In detail, the changes are:

  - add sanity check for the detected stack limits
  - fixed: one some systems rkward would crash or not compile due to
    missing fortran symbols
  - fixed: starting the scatterplot plugin in wizard mode would crash
    rkward
  - tab-completion in the console will do partial completion, if all
    potential completions have a common start
  - fixed: file-name completion would always assume the home directory
    as the current directory
  - update turkish translation
  - compilation fix for FreeBSD (thanks to Thierry Thomas)
  - fixed: when executing commands line by line from the script editor,
    line breaks would be omitted

If you are experiencing any of the above problems, please
[download](Download_RKWard.html) and upgrade to version 0.4.7a of
RKWard.

### RKWard 0.4.7 released

*April 11 2007 - [User:Tfry](User:Tfry.html)*

Version 0.4.7 of RKWard is now available for
[download](Download_RKWard.html).

Once again the list of new features and improvements is long. Most
importantly, a large number of plugins has been added, making more R
statistics features available via graphical dialogs. Many thanks to all
who contributed to this release\!

The changes in detail:

  - New and improved plugins:
      - t-test plugin now also allows to calculate a paired test
      - new plugin: Phillips-Perron test
      - new plugin: Mood test
      - new plugins: CLT plots for most distributions
      - new plugins: variance tests (F test (var.test), Bartlett test,
        Fligner test)
      - new plugin: generic plot
      - new plugin: Add grid to plot
      - new plugin: pareto chart
      - new plugin: stem-and-leaf plot
      - new plugins: outlier tests (chisq.out.test, dixon.test,
        grubbs.test, outlier)
      - new plugins: support of all tests from moments package
      - import SPSS and import CSV plugins gain option to open object
        for editing, automatically (checked by default)
      - several existing plugins have been improved in various ways
  - Bug fixes:
      - some fixes for GCC 4.3
      - fixed: the presence of user objects called "missing", "assign",
        or "get" would confuse some RKWard internals
      - fix a crash that occurred during startup under some
        circumstances
      - when flushing the output, also delete all graphs
      - make sure not to run parts of an incomplete user command. It
        would lead to all sorts of subtle problems
      - fixed: changing the type of a column in the data editor was
        broken
      - fixed: quitting while a plugin was still active would sometimes
        crash rkward
      - better error handling in PHP backend
      - fixed: depending on some buffer sizes, plugins could get stuck
        while generating code
      - fixed: plugin browser type="dir" would not work correctly
      - fixed: graph previews would stop working when the interface is
        switched from dialog to wizard or vice versa
  - Other user visible improvements:
      - use maximum permissible width in the object viewer
      - show current working directory in the statusbar
      - add run selection action to all HTML windows
      - add custom icons for run line/selection/all
      - if an incomplete command was piped through the console, the
        console does not become blocked for piping
      - make "run current line" advance the cursor to next line after
        running
      - warnings and errors are highlighted in command log / console
        (warnings only for R \>= 2.5.x)
      - also support filename completion in the console (mode of
        completion is determined by heuristic means)
      - the currently active window is now highlighted using a thin red
        border
      - new shortcuts for window navigation: next/previous window,
        activate console, activate command log, etc.
      - make readline dialog store last size and position
      - several new help pages
      - warn when opening very large objects (with more than 250000
        fields; this limit is configurable)
      - new options for plugin dialogs: Configure whether code display
        is shown by default, and at what size
  - Internal improvements:
      - add option to enable / disable single options of a radio or
        dropdown control in plugins
      - new functions rk.describe.alternative () and rk.print.literal ()
        for use in plugins
      - in help files, a short title for sections (for the navigation
        bar) can be defined
      - add support for a "technical details" section in plugin help
        files
      - initial support for R's Sys.setlocale(), and encoding of
        CHARSXPs
      - plugin generated commands are run in a local() environment,
        allowing for more concise code
      - support R's mechanism for checking the C stack limits (only with
        R 2.3.x or greater, and not on all platforms)
      - add a "copy" tag to facilitate writing plugins with both dialog
        and wizard interfaces

We're looking forward to your feedback on the [mailing
list](Contact.html).

### New mailing list: rkward-users

*April 04 2007 - [User:Tfry](User:Tfry.html)*

The past few months there has been a lot of active development on
RKWard. The outcome of this - version 0.4.7 - is scheduled for release,
next week. As a side effect, our main mailing list has become rather
busy, with lots of discussion about development details. At the same
time, the stats show a nice number of downloads, and hence a
considerable increase in the RKWard user base.

For these reasons, it's time to create a dedicated users' mailing list.
This list will be read by most active developers, and it will receive
notifications about the most important steps of development (new testing
versions and releases). However, instead of discussing low level
details, this list will provide a better forum for discussing usage
questions, but also for providing suggestions and feedback.

If you'd like to keep informed on all development details (and perhaps
provide insights and contributions on that), you may want to subscribe
to our "old" development mailing list. If you're mostly interested in
questions of daily use, the new users' [mailing
list](Contact.html) is the best choice. Or subscribe to both, if
you don't want to miss a thing.

### RKWard 0.4.6 is out

*February 15 2007 - [User:Tfry](User:Tfry.html)*

Version 0.4.6 of RKWard is available for
[download](Download_RKWard.html).

Most prominently, this release features many new plugins, making more of
the power of R available via graphical dialogs. A big thank you goes to
all volunteers who contributed to this release. The full list of
changes:

  - New and improved plugins:
      - support for calculating p-values in correlation matrix plugin
      - simplified CSV import plugin
      - new plugin: import SPSS files
      - various improvements to existing distribution and distribution
        plot plugins
      - new plugins: distribution plots: beta, binomial, cauchy,
        exponential, gamma, geometric, hypergeometric, logistics,
        lognormal, negative binomail, tukey, uniform, weibull, wilcoxon
      - new plugin: Ansari-Bradley two-sample test
      - support for preview functionality in graphing plugins
      - new plugin: barplot
      - new plugin: export contents of an X11 device to several
        different file formats
      - plugins can now be context sensitive (e.g. work on a particular
        x11 device)
      - new plugin: Wilcoxon Rank Sum test and Wilcoxon Exact Rank Sum
        test
      - new plugin: scatterplot matrix
      - new plugin: correlation matrix plot
      - new plugin element to select name of an R object to save to
      - add possibility to use dropdown lists in plugins
      - improvements to spinbox: step size is adjusted dynamically, and
        no arbitrary limits in real mode
      - fixed problem with specifying y axis limits in several plugins
      - single line input fields in plugins no longer accept new-lines
      - pressing tab changes focus while in a single line text input
        field
  - Other new features and improvements:
      - new documentation pages: console
      - place internal objects .rk.rkreply and
        .rk.available.packages.cache into rkward package environment
      - safer destruction of R backend on quitting
      - show full error message for syntax errors (only if compiled with
        R 2.4.0 or newer)
      - add ability to search command history context sensitively
  - Bugfixes and corrections:
      - make sure any LC_ALL environment setting will not be applied by
        SCIM plugin (if installed on the system)
      - fix crash after changing storage type of an object in the data
        editor
      - fixed: pressing F8 in the console would remove input focus from
        the console
      - fix several small problems with detached windows
      - fix a problem with keeping the list of objects up to date
      - fixed: installing packages as root would not work, when $R_HOME
        is not defined
      - fix compilation for the upcoming R 2.5.0
      - more correct auto-printing of values for user commands (only if
        compiled with R 2.4.0 or newer)
      - more correct handling of user commands consisting of several
        statements (only if compiled with R 2.4.0 or newer)

For feedback on the new release, and RKWard in general,
[contact](Contact.html) us on the mailing list.

### RKWard 0.4.5 is released

*January 21 2007 - [User:Tfry](User:Tfry.html)*

Once again, it's time to announce a new release of rkward. Version 0.4.5
is available for [download](Download_RKWard.html).

Note that a few aspects of the user interface have changed for this
release. We hope these changes will make RKWard's user interface easier
to learn and use, but existing users will have to adapt to the following
UI changes:

  - changed shortcuts for Open/Save Workspace to Ctrl+Shift+O/S to avoid
    clash with Open/Save Script File
  - removed "Configure Packages" option from Workspace menu, as it is
    also in Settings menu
  - removed the "output" menu; entries were moved to "Edit" and "View"
  - added refresh output and flush output as toolbar actions
  - always show all top level menus

Further changes in RKWard 0.4.5:

  - Bugfixes / corrections:
      - fixed: non-local workspaces would not be loaded
      - fixed: specifying a workspace to load on the command line would
        only work for full urls
      - fix: the output window would not refresh changed plot images
        (unless closed and reopened)
      - fixed: integer spinboxes with starting values greater than 1
        would get unreasonable change steps
      - fix several minor issues as identified by the "krazy" source
        code checker
      - small improvements to syntax highlighting definition
      - fix compilation with some versions of KDE / GCC
      - assorted cleanups
  - Plugins:
      - added plugin for skewness and kurtosis
      - added several plugins for distribution analysis
      - added plugins for stripchart & density plot
      - enhancements to existing plot plugins
      - code generated for most plugins cleaned and simplified
      - new function rk.results () for use in plugins
  - Other new features / improvements:
      - new Console RMB actions: "copy selection cleaned" and "run
        selection"
      - added a basic help system, and some introductory help pages
      - allow varslots to request objects of specified dimensionality
        and length
      - in console, make cursor go to empty next line after pressing
        enter, before there is output (more like in the real console)
      - add "run selection" option to command log
      - script editor commands are piped through the console (by
        default; this is configurable)

Got feedback about the new release or RKWard in general? Please contact
us on the [mailing list](Contact.html).

## Archived news 2006

### RKWard 0.4.2 released

*December 04 2006 - [User:Tfry](User:Tfry.html)*

A new release of RKWard is available for
[download](Download_RKWard.html), today. The new version has seen
many small changes all over the place. Among the more noteable additions
are some new and improved plugins, new and updated translations, and
also R X11 device windows are now integrated into the rkward GUI with
some added functionality.

The list of changes in 0.4.2:

  - Bugfixes / corrections:
      - fix help menu for detached windows
      - fixed: selecting Window-\>Close in a detached window would only
        close the internal view, not the window
      - when detaching a window, give it a more reasonable size
      - do not open the same script url twice (instead the corresponding
        window is raised on the second attempt)
      - fixed: when closing a detached script editor window, you would
        not be asked to save changes (if any)
      - fixed: occasional crash when re-attaching a script editor window
      - fix bug in rk.get.label ()
      - properly support active bindings in globalenv ()
      - if not started in a tty, spawn a konsole. R will go into
        non-interactive mode otherwise
      - in script editor, do not provide symbol name completion while in
        comments
      - spelling corrections in distributions menu
  - Translations:
      - new translations: Greek, Catalan
      - updated translations: German, French
  - Feature additions and improvements:
      - code generated by scatterplot plugin is simplified somewhat
      - add comment headers to the sections of commands produced by
        plugins
      - if a user command results in the output html file to be
        modified, auto-refresh output
      - add RMB menu to script editor windows
      - add options "clear" and "configure" in the console RMB menu, and
        the command log RMB menu
      - add option to save workplace layout not per workspace, but at
        the end of the session
      - add options ("printcmd") to settings
      - R X11 device windows are now managed by rkward, with the
        following initial features:
          - R X11 device windows: attach to / detach from workplace
            (they start detached)
          - R X11 device windows: toggle between normal resize mode and
            (settable) fixed size with scrollbars
          - R X11 device windows: menu options to copy / print /
            duplicate / save / activate devices
      - added basic plot facility for ECDF (Empirical Cumulative
        Distribution Function) (menu plots)
      - added ptukey and qtukey (Studentized Range Distribution (Tukey))
        (menu distributions)
      - reorganized "Descriptive Statistics" plugin to use tabs for
        better usability
      - added constant for mad in descriptive statistics plugin

Obligatory Douglas Adams reference for this version number: RKWard won't
give you the answer to all questions, yet, but it does compute 6\*7
reliably. So try the new release, and send your feedback to the [mailing
list](Contact.html).

### RKWard 0.4.1: bugfixes, and code completion

*November 06 2006 - [User:Tfry](User:Tfry.html)*

A new version of RKWard is available for
[download](Download_RKWard.html). Most importantly, a good number
of bugs has been fixed, and RKWard gains code completion and function
argument hinting features in console and script editor. The complete
list of changes:

  - Bugfixes:
      - fix bug in beta probabilities plugin
      - fix (some?) compilation problems with KDE 3.2
      - fix to plugin input lines sizing, and visibility of scrollbars
      - prevent infinite recursion when parsing structure of
        environments
      - fix for compilation on 64bit architectures
      - don't crash when closing a detached window
      - fixed: an empty table created before the object list was updated
        (esp. at startup) would be thought to have been removed in the
        workspace
      - fixed: back / forward buttons would sometimes remain after
        closing a help window
      - fix some focus problems
      - remove misleading "About KDE" dialog, and replace report Bug
        dialog with correct information
  - New features and improvements:
      - allow specification of title/subtitle in plot options plugin
        (currently used by boxplot, and histogram)
      - when packages have been newly installed, show them in the load /
        unload packages tab immediately
      - on startup, show window maximized if no size stored (i.e. very
        first startup)
      - function argument hinting in the console and script editor
      - as you type completion of R symbol names in the script editor
      - tab completion of R symbol names in the console
      - added R function rk.edit (x) to open object x for editing in
        rkward
      - object data changed in the console (or by other means) is
        updated to the editor automatically, if the object is opened for
        editing
      - provide close buttons in tab bar

As usual, we're interested in your feedback on the [mailing
list](Contact.html).

### RKWard 0.4.0 released

*October 17 2006 - [User:Tfry](User:Tfry.html)*

Once again, it's time to announce a new release of RKWard is available
for [download](Download_RKWard.html). Perhaps most importantly,
this release enables you to browse the entire R workspace, including
objects in R packages. In future releases this will allow us to
implement helpful features such as code completion and argument hinting,
so stay tuned for what is still to come.

The list of changes in 0.4.0:

  - Features / improvements:
      - many new distribution plugins
      - all distribution plugins now use a free-text field, allowing you
        to enter a vector of probabilties / quantiles
      - RMB option to search help on objects from packages in object
        browser
      - show tooltip information on objects in object browser
      - the object browser is updated automatically when needed
      - allow some more configuration in object browser
      - include all package environments in object list
      - add (ugly) items for functions and lists to object browser
      - when saving/loading the workspace, save / restore all windows
        (not just data editors)
      - add mismatching brace detection to syntax highlighting
      - RKWard should now be fully UTF-8 aware. All characters are
        passed to and from R reliably
  - Bugfixes:
      - another fix to cancelling during readline () calls
      - do not crash on call to "browser ()"
      - remember workspace url after save as
      - fix a long standing (but hard to trigger) crash while closing
        data editor window
      - fix "Open R Script File" filename ending filter
      - yet another crash on more than 100 rows bug fixed
  - Internal changes:
      - invalid values are stored in a separate attribute instead of
        changing the storage mode back and forth
      - storage mode for RKWard meta data was changed (see note below)
      - internal cleanups (partially to make porting to KDE4/Qt4 easier)
      - use more efficient data passing to synchronize object list

Please give the new release a try, and send your feedback to the
[mailing list](Contact.html).

**Note:** The way meta information (most importantly the descriptive
variable labels in the data.frame editor) is stored was changed in this
release. When loading workspaces created with earlier versions of
rkward, this information will not be available (the data is not
affected, however). To convert your saved workspaces to the new format,
1) open the workspace in question, 2) run the command "rk.convert.pre040
()" in the RKWard R console. We're sorry about the inconvenience.

### RKWard 0.3.7 is out

*September 17 2006 - [User:Tfry](User:Tfry.html)*

A new release of RKWard is available for
[download](Download_RKWard.html). This release contains mostly
small changes all over the place, but also fixes some important issues.
The changes in detail:

  - Bug fixes:
      - small fixes in several plugins
      - avoid lockups when cancelling a readline ()
      - don't crash on more than 100 rows of data in the editor (off by
        2 bug in storage allocation)
      - correctly initialize the internal NaN representation (would
        sometimes be initialized to 0, resulting in 0s not to be shown
        in the editor)
      - commands producing lots of output would sometimes be impossible
        to interrupt. Fixed
      - sometimes the last level in the factor levels editor got lost.
        Fixed
      - fix spurious line breaks in output in command log and console
  - Package installation / updating:
      - packages can be installed to custom library locations
      - semi-automate package installation in response to "require ()"
        calls
      - the online list of available packages is cached up to one hour
        (and only as long as the relevant options are unchanged)
      - give better progress indication
      - package dependencies are handled correctly when installing new
        packages
  - Showing command output:
      - show output context (if any) when the backend asks for input via
        readline ()
      - do not raise the command log more than once for a given command
        producing continuous output
      - deal better with commands producing large amounts of output
      - show command output immediately in command log
  - Compilation:
      - link against libRblas.so if it is found in the R libs dir
        (needed for some compiles of R 2.4.0)
      - fix compilation with GCC 4.1
      - honor DESTDIR setting while installing rkward R package, and add
        configure option --with-r-libdir
      - only link against -lRlapack, if that is available
  - Assorted changes:
      - Improved syntax highlighting defintion including code folding
      - engine status bar label ("R engine busy/idle") now changes
        background color according to state
      - small improvements to command history in console
      - remember location where R script/workspace files were last
        opened, and start file-selection dialogs there
      - remove whitespace from start and end of plugin generated
        commands and do not evaluate empty commands
      - added Turkish translation

RKWard always needs user feedback to improve further, so please try out
the new release, and tell us what you think on the [mailing
list](Contact.html).

### Bugfix release 0.3.6

*April 23 2006 - [User:Tfry](User:Tfry.html)*

Much sooner than expected, a new release of RKWard has been made
available for [download](Download_RKWard.html). Most importantly,
contrary to our expectations of one week ago, RKWard 0.3.5 will not work
with the upcoming R 2.3.0, so a new release was needed quickly.

Despite the limited time, a number of further bug-fixes and small
improvements have made it into the new release. Here's a mostly complete
list of changes:

  - Compilation / Installation:
      - remove configure options --with-r-share and --with-r-doc
        (obsoleted by improved wrapper script)
      - some fixes for R 2.3.0
  - Bugfixes:
      - clean up output of require (quietly=FALSE)
      - middle mouse pastes selection, not global clipboard in the
        console, as expected
      - make Ctrl+V work in the console
      - fix bug in scatterplot plugin
      - don't try to show higher numeric precision than is actually
        available
      - use KDE standard icons in object lists (need to be replaced by
        customized icons in the long run)
      - check for and handle some strange R parse errors that could
        crash rkward
      - several fixes to the distributions plugins
  - Improvements / features:
      - make Ctrl+C/copy work in output and help windows
      - allow empty input in readline ("R backend requests information")
        dialog
      - process R X11 events while modal dialogs (such as "R backend
        requests information") are shown
      - pressing Cancel in readline ("R backend requests information")
        dialog interrupts the command
      - prevent R-Object listviews (Varselector, Workspace Browser) from
        growing excessively wide
      - allow deletion of several rows at once in the data.frame editor
        and fix some bugs regarding deletion of rows
      - make Ctrl+C copy work in the code window of plugins
      - remove one level of menu nesting for save/load R Objects plugins
  - Internal changes:
      - remove debian dir from source distribution again (for better
        packaging)

As usual, we'd be interested to read your feedback on the [mailing
list](Contact.html).

**Important Note:** RKWard 0.3.6 can be compiled (and of course run)
with R 2.1.x, R 2.2.x and the upcoming R 2.3.x. However, when upgrading
from R 2.2.x to R 2.3.x, you will have to re-compile and re-install
RKWard. Also, if your distribution currently ships a pre-release version
of R, chances are, RKWard will not yet work with that version (but you
may want to give it a try). You'll have either downgrad to 2.2.x
temporarily, or upgrade to the final release of R 2.3.0 once available.
To be precise, SVN revision 37824 is the first pre 2.3.0-version of R,
RKWard 0.3.6 will work with.

Currently a pre-compiled .deb package of RKWard is provided for R 2.2.x
releases. A .deb package compiled for R 2.3.0 will be made available as
soon as possible.

We're very sorry about the inconvenience, but unfortunately, there is
nothing we can do about it.

### RKWard 0.3.5 is out

*April 16 2006 - [User:Tfry](User:Tfry.html)*

Finally a new release of rkward is available for
[download](Download_RKWard.html). The focus of this new release
has been on reworking the code responsible for dealing with plugins, and
on fixing many small bugs. However, also a number of new
features/improvements have been done since the last release. Most
noteably, there are new plugins for Histograms and Boxplots, and for
many distribution functions. A mostly complete list of changes:

  - Bug fixes:
      - give back focus to the script editor after running
        line/selection/all
      - clean up R temporary files on shutdown
      - fix crash with objects with quotes in their names
      - avoid leftover PHP processes if rkward crashes or gets killed
      - code display (for plugins) gets updated reliably
      - handle non-existing and non-local URLs
      - use a shell wrapper around rkward to do some setup just like R.
        This should fix a number of minor quirks (links) in help window
      - update tab caption for command editor if filename changes (i.e.
        saved as)
      - keep main window caption up to date
      - fix status bar
      - in trailing rows/columns of the data.frame editor, change
        background color, when selected
      - explicitely set LC_NUMERIC to "C" at startup. R does not work
        correctly with other settings of LC_NUMERIC
  - New features:
      - added syntax highlighting in the console
      - show info message, if retrieving context help fails
      - pressing Ctrl+C in the console resets a partial command, if any
      - backspace or del clear cells in data.frame editor
      - when switching between dialog and wizard interfaces, GUI
        settings are copied
      - add options to show help-search and R help index to Help menu
      - many new plugins by Stefan Roediger
      - indicate if a document in a command editor is modified (unsaved)
  - Compilation / installation:
      - configure: if not overriden, and $KDEDIR is not set, assume
        --prefix=\`kde-config --prefix\` instead of /usr/local/kde/
      - autodetection of R_HOME and related variables during configure
      - configure: use single configure option for location of R_HOME,
        and add check for libR.so
      - most of the time you will no longer need to specify options for
        ./configure
  - User Interface improvements:
      - reworded exit dialog box
      - remove some GUI clutter for Script File editor
      - rename "Command File" to "Script File"
      - rename "Command Stack" window to "Pending Jobs"
      - reworked factor levels editor
  - Assorted:
      - complete rework of plugins code
      - plugins can embed other plugins
      - yet more code cleanups

Please give the new release a try, and send your feedback to the
[mailing list](Contact.html).

**Important Note:** If you see the message "Error: C stack usage is too
close to the limit" on the console, this is due to a small but grave bug
in R. This bug is present only in a few pre-release packages of R 2.3.0,
but some of those are floating right now. If you encounter this bug, you
will have to either downgrade your version of R, or wait until fixed
packages are available for your distribution. If in doubt what to do,
ask us for advice on the mailing list.

## Archived news 2005

### RKWard 0.3.4 released

*November 2005 - [User:Tfry](User:Tfry.html)*

A new version of RKWard is now available for download. Changes include:

  - Bug fixes:
      - fix installation of standard_plugins.pluginmap
      - make "up"-button work in R-backend repository settings
      - Settings-\>Command Log-\>show/raise window working again
      - assorted other bugfixes, including fixes to linkage, fixes for
        crashes, etc.
  - New features / improvements:
      - real time display of command output in the console
      - automatically scroll command log to the bottom when new lines
        are added
      - back/forward navigation in help pages
      - can print help pages and output
      - better handling of errors while installing packages
      - allow loading/saving of command history (console commands)
      - allow interrupting current command running in the console via
        Ctrl+C
      - add "Command stack" window for advanced control of R backend
        (cancel commands, pause chain, view stack)
      - allow configuration of many R options in Settings-\>R backend
      - avoid adding incomplete (multi-line) Console commands to the
        Command Log
  - User interface:
      - less verbose output in command log (removed "issuing command",
        etc.)
      - help-, output-, and editor-views can be detached to separate
        windows
      - allow to bypass startup dialog
      - limit number of lines to display in console and command log
        (user setting)
      - remove menu clutter from help window, and output window
      - add (meaningful) context menu for console
  - Translations:
      - updated French translation
  - Internal / technical changes:
      - include debian-dir in source distribution
      - use KXMLGUI for placing plugins in the menu (mostly an internal
        change, but menu placement should remain sane, when active
        window changes, now)
      - modifiy rkward.desktop to always start rkward in a terminal. The
        backend sometimes behaves strangely, if this is not done
      - use available.packages instead of CRAN.packages (deprecated in R
        2.2.0)
      - more code cleanups

Please check out the new release, and tell us what you think on the
[mailing list](Contact.html).

**Note:** If you have previously been running version 0.3.3 of RKWard,
you may have a bad saved setting for the location of .pluginmap-files.
In this case RKWard will complain about missing plugins on startup. In
order to fix this, go to Settings-\>Configure RKWard-\>Plugins and
remove all entries for .pluginmap-files. After that, quit, and restart
RKWard. The correct location of the standard plugins should then be
autodetected.

### Bug in RKWard 0.3.3

*October 12 2005 - [User:Tfry](User:Tfry.html)*

One fairly important bug went unnoticed, when preparing the 0.3.3
release. An important file did not get installed, and this will lead to
RKWard being unable to find its plugins. Fortunately, this can be worked
around by simply installing one additional file.

Download [this
file](http://cvs.sourceforge.net/viewcvs.py/*checkout*/rkward/rkward/rkward/plugins/standard_plugins.pluginmap?rev=1.3).
It needs to be installed to
$prefix$/share/apps/rkward/standard_plugins.pluginmap, where $prefix$
is the directory you specified as --prefix when running ./configure. So,
for example, for debian, it has to be installed to
/usr/share/apps/rkward/standard_plugins.pluginmap

Sorry about the inconvenience.

### RKWard 0.3.3 is released

*October 10 2005 - [User:Tfry](User:Tfry.html)*

Together with the new website, a new version of rkward is now available.
Changes in the new release can be grouped roughly in these categories:

  - Feature additions: Options to archive downloaded CRAN packages, and
    to select custom repositories. Context help in the console via F2.
  - User interface: The user interface is now somewhat more consistent.
    For instance, all windows can be closed with Ctrl+W, and the menu
    options get updated according to the current window. New icon by
    Marco Martin
  - Backend enhancements: A number of internal changes have been made.
    What should be noteable to regular users is that RKWard now behaves
    sanely, if you run commands like "fix (myfunction)" or "help
    (print)". Also warnings are now displayed correctly in most cases.
  - Plugins: A first step has been done to rework the plugin
    infrastructure. The next release will see further improvements
  - Cleanups & Bugfixes: A number of bugs has been fixed, many sections
    of dead code have been removed, several areas of the sources are
    much cleaner, and more well-documented, now.

For a complete list of changes, please see the changelog in the
[download](Download_RKWard.html) section. Please give the new
release a try. We welcome all feedback to the [mailing
list](Contact.html).

### New website for RKWard

*October 10 2005 - [User:Tfry](User:Tfry.html)*

Unless you're here for the first time, you'll have already noted:
Finally the old RKWard website has been replaced with a new, pretty, and
flexible website. A very big thank you goes to Yves Jacolin, the one who
worked hard to create it\!

The new website is divided into three main sections, you can navigate
between using the links on the top of each page. The User section
contains usage information on RKWard, screenshots, and FAQ. Developers
will find additional information in the Development section. Finally, a
new Wiki is available, where users can help us create new documentation
on RKWard, but also discuss translation, and development issues.

Also, you will note that the new website is designed to be
multi-lingual. So far, little has actually been translated, but we will
work hard on providing the most important information in several
languages (work has started on a French translation). If you can help
with this, please contact us on the mailing list. Old news items - tfry
- On the 07 October 2005 -

With the new website layout, we switched to our own news system. This
new system allows us to post news in a number of different categories,
and filter them according to those categories - such as news of interest
to users vs. news for developers. Also, news can now be translated.
Older news items, posted prior to the switch, are still available from
the sourceforge archive.
