---
layout: page
---

# Download RKWard

## Official source releases

The latest official source packages. See [Building RKWard From Source](Building_RKWard_From_Source.html) for help on compiling.

- **[rkward-0.7.4](https://download.kde.org/stable/rkward/0.7.4/rkward-0.7.4.tar.gz)** (released May 30, 2022)
- [ChangeLog](https://invent.kde.org/education/rkward/-/blob/releases/0.7.4/ChangeLog)

## Compiled packages and system specific instructions

Ready-made binaries or hands-on instructions are available for many
different setups:

### Linux / Unix / BSD binaries

Please refer to this page for more info: [Binaries and Build Scripts](Binaries_and_Build_Scripts.html).

### Windows Installer

See [RKWard on Windows](RKWard_on_Windows.html) for details.

### Mac OS X Installer

See [RKWard on Mac](RKWard_on_Mac.html) for details.

## Current test releases

Please refer to our [Release Schedule](Release_Schedule.html) for
links to current test releases. These are source packages which are
currently undergoing testing. We'd like to encourage you to give these a
try, and [report](Contact.html) issues, but keep in mind, that
these packages have only received limited testing, so far.

## Development sources

See [Instructions on using the Source repository](RKWard_Source_Repository.html)

## Additional plugins

RKWard can be enhanced by installing [additional plugin packages](Third_Party_Plugins.html). They come in format of
ordinary R packages and are hosted on our plugin repository.

## Archive

Historical source releases are available from <https://download.kde.org/Attic/rkward/>, and - for even older releses - <http://sourceforge.net/projects/rkward/files/Historical_Releases/>.
