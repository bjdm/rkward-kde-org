---
layout: page
---

# You can help
There are a lot of ways to help this project, not just as a programmer, but also by providing feedback, writing documentation, translating, creating statistical plugins, or by [donating](http://sourceforge.net/donate/index.php?group_id=50231).

Whatever it is that you can contribute, here are some first pointers to help you getting started. As the very first pointer, however: Never shy away from [contacting](Contact.html) us! 

## Important starting points for all contributors
  - [RKWard Source Repository](RKWard_Source_Repository.html): For contributions of all kinds (code, documentation, translations, plugins) it is highly useful to keep track of the current state of development, not only when offical versions are released.
  - [Submitting Patches](Patches.html)
  - [Release Schedule](Release_Schedule.html)

## Tracking bugs, tasks and feature suggestions:
  - Submitting, reviewing or browsing [Bugs and Feature Requests](Bugs.html)
  - [Open Tasks](Open_Tasks.html)
  - [Feature Plan](Feature_Plan.html)

## Documentation for documentation writers
  - [The sources for this website](https://invent.kde.org/websites/rkward-kde-org)
  - Info on the documentation format used in RKWard's internal documentation (the "Welcome"-pages but also Help pages for individual plugins): <https://rkward.kde.org/doc/rkwardplugins/pluginhelp.html>

## Translators
  - [Getting started with translating RKWard](Translation_RKWard.html)

## Developing plugins aka "Statistics Dialogs"
  - Hands-on tutorial and reference on writing plugins: [HTML for online browsing](https://rkward.kde.org/doc/rkwardplugins)
  - [Automated Plugin Testing](Automated_Plugin_Testing.html)

## C++-coders
  - The appendix to our article in the Journal of Statistical Software discusses several aspects of the techical design, and is still mostly relevant: ([Abstract](http://www.jstatsoft.org/v49/i09), [PDF](http://www.jstatsoft.org/v49/i09/paper)).

## Further resources
### Mailing list archives
Has something been discussed, before? Check the mailing list archives:

  - <https://mail.kde.org/pipermail/rkward-devel/> (RKWard-Devel)
  - <https://mail.kde.org/pipermail/rkward-users/> (RKWard-Users)
  - <https://mail.kde.org/pipermail/rkward-tracker/> (RKWard-Tracker)

### Outdated pages
Really, you should not be reading those, except when thinking about how to replace these:
  - [Development Status of Plugins](Development_Status_of_Plugins.html)
  - [Plugins under planning](Plugins_under_planning.html)

### External trackers
HThere are a number of further places where bugs / issues are
reported. While these trackers are distribution-specific, many of the
issues reported there concern RKWard in general. Therefore it makes
sense to a least check these once in a while:

  - Ubuntu: <https://bugs.launchpad.net/ubuntu/+source/rkward>
  - Debian: <http://bugs.debian.org/cgi-bin/pkgreport.cgi?package=rkward>
  - Fedora: <https://bugzilla.redhat.com/buglist.cgi?component=rkward&product=Fedora>
