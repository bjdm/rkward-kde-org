---
layout: page
---

# Open tasks

This page lists a number of areas that need work. If you would like to
get involved in RKWard, but don't have a specific idea on where to
start, this list is for you. Note that we basically require people of
all skill-sets.

This list is not meant to be a complete list of wanted features. Rather,
these are some tasks that may be good starting points for new
contributors.

Along with each task I outline some ideas I have about the component in
question, but of course you don't need to follow those, and can bring in
your own ideas. Taking a task does not mean you're 100% responsible for
all aspects of it - do whatever you feel interested in and able to do.
Also I specify the main skills required for working on this task. If
you're interested in picking up some of these tasks, please [Contact
us](Contact.html).

## Usability / GUI improvements

Skills required: Evaluating usability aspects.

The RKWard GUI is rather inconsistent, and there are a number of
dialogs, etc., that could certainly be improved usability wise. Pick
whichever dialog/aspect annoys you most, and come up with a suggestion
on how the user interface could be redesigned to improve usability.
Provide a description how the interface should work, and maybe provide a
mock "screenshot" of what it should look like. Of course, if you have
coding-skills, you may even submit a patch, but that's not required.

## Adding "What's this" help, and/or "Tip of the Day"-functionality

Skills required: some C++-coding. Some experience with Qt recommended

"What's this" help are the help popups provided, when you click on the
"?"-icon in a window, and then select a GUI-element. Currently RKWard
does not have any of them at all. Adding does not require much
programming knowledge, though you should have a basic understanding of
C++-syntax. The nice thing about this task is that it concerns many
areas of the sources, and so you may easily get a good insight into the
code, while contributing helpful code at the same time.

"Tip of the Day" should be fairly easy to implement in C++, and may be
an ideal task to get started with coding on RKWard. Of course the tips
themselves will need to be written, as well, but that a separate step.

## R Command Log

Skills required: C++-coding. Experience with Qt recommended

The Command Log is the tool window that shows the commands which have
been run and their results (by default on the lower left corner). This
could be enhanced in several ways. First, you should be able to filter
out some kinds of commands, dynamically, instead of only from the
Settings. Commands which belong to the same chain (e.g. computation of
an analysis and then formatting of the output) should be recognizable as
such. Second you should be able to save the logs to a file (and probably
that should be the primary storage, with the visible log only a display
widget, not a storage container). Also it would be nice to be able to
e.g. have popup-menus per command, that allow you to re-run the command
in question. It would be good to be able to spawn several command logs
with different filter settings, etc.

Source-files to look at: windows/rkcommandlog.cpp and
rbackend/rcommand.cpp

## Output Window

Skills required: C++-coding. Some experience with KDE/Qt recommended

A lot will have to happen here, and probably it will one day look very
different than it does today. What I think are small yet important items
are: Loading/saving/clearing of output; use of CSS for formatting;
navigation bars, etc. Importantly, it should be possible to use more
than just a single output window / file, and to switch output to a
different window / file. Currently a khtmlpart is used for displaying
the output. Another important sub-task is to come up with ideas how
output should best be handled in general, what sort of markup-tags are
needed, etc.

Main class to look at (as a starting point): RKHTMLWindow.

## Writing Plugins

Skills required: some knowledge of R. Minor experience with XML or
related languages (e.g. HTML) recommended

Plugins are the core of RKWards functionality, and we need much more to
come in this area.

Documentation to look at: [Writing Plugins](/doc/rkwardplugins/)

## Documentation writing

Skills required: Wiki editing, writing documentation, in-app help pages

We need more documentation, particularily [User
Documentation](User_Documentation.html). Can you help?

## Website administration

Skills required: PHP; basic system administration

Our website needs love. [Contact us](Contact.html), if you can
help.

## Testing/reporting bugs/suggestions

It can't be stressed enough: Even if you don't have any of the skills
required for the above tasks, you can make a valuable contribution by
providing feedback. Spot usability problems before they become hard to
fix. Report the most annoying bugs the developers no longer notice. Tell
us about the features you would like to have added/changed/removed.
Participate in discussions on the mailing list.
[Contact](Contact.html)

## Marketing

The larger the user base of RKWard, the sooner bugs will be reported,
and fixed, the more potential developers will become aware of the
project, the faster RKWard will evolve. Don't go around spreading
marketing lies, but recommend trying out RKWard to your
friends/colleagues. Create new and better screenshots for the homepage,
kde-apps.org, and freshmeat. Add RKWard to software listings that don't
have it, yet. Suggest to your distribution vendor to include RKWard in
their archive.

## More

  - This mail lists a number of issues to address (TODO merge into this
    page)
    <http://www.mail-archive.com/rkward-devel@lists.sourceforge.net/msg00221.html>
  - [Feature Plan](Feature_Plan.html)

