---
layout: page
---

# RKWard scripts

Here some script example to test in RKWard

### Creating 3D plot

`# Author : Yves Jacolin`
`# 06/06/2005`
`# source ("R_test.R")`
`x<-seq(-10,10,0.2)`
`y<-seq(1,50,0.5)`
`y<-y/15`
`z<-outer(x,sin(y))`
`fill<-matrix("green3", nr=nrow(z)-1, nc = ncol(z)-1)`
`fill[,i2<-c(1, ncol(fill))] <-"gray"`
`fill[i1<-c(1, ncol(fill)),] <-"gray"`
`fcol<-fill`
`fcol[]<- terrain.colors(nrow(fcol))`
`persp(x,y,z,theta=320, phi=30, expand=0.3, col=fcol, ticktype="detailed", shade=0.05, zlab="Altitude", ltheta=-120,box=FALSE)`
